﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class InvitationModel : IGroupInvitation
    {
        public long UserId { get; set; }

        public long GroupId { get; set; }

        public DateTime InvitationSent { get; set; }

        public InvitationModel()
        {

        }

        public InvitationModel(IGroupInvitation group)
        {
            this.UserId = group.UserId;
            this.GroupId = group.GroupId;
            this.InvitationSent = group.InvitationSent;
        }
    }
}
