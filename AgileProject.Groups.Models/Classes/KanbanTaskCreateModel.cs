﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class KanbanTaskCreateModel : IKanbanTaskCreate
    {
        public long GroupId { get; set; }

        public long PerformerId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public TaskState State { get; set; }

        public KanbanTaskCreateModel()
        {

        }

        public KanbanTaskCreateModel(IKanbanTaskCreate task)
        {
            this.GroupId = task.GroupId;
            this.PerformerId = task.PerformerId;
            this.Title = task.Title;
            this.Description = task.Description;
            this.State = task.State;
        }
    }
}
