﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class KanbanTaskModel : KanbanTaskCreateModel, IKanbanTask
    {
        public long Id { get; set; }
        public long CreatorId { get; set; }

        public KanbanTaskModel()
        {

        }

        public KanbanTaskModel(IKanbanTask task) : base(task)
        {
            this.Id = task.Id;
            this.CreatorId = task.CreatorId;
            
        }
    }
}
