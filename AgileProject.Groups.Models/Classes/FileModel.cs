﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class FileModel : IFileModel
    {
        public long Id { get; set; }

        public long GroupId { get; set; }

        public string Name { get; set; }

        public long CreatorId { get; set; }


        string IFileModel.NameForUsers => this.Name;

        

        public FileModel()
        {

        }

        public FileModel(IFile file)
        {
            this.Id = file.Id;
            this.GroupId = file.GroupId;
            this.Name = file.NameForUsers;
            this.CreatorId = file.CreatorId;
        }
    }
}
