﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class TaskCommentModel : KanbanTaskCommentCreateModel, ITaskComment
    {
        public long Id { get; set; }

        public long CreatorId { get; set; }

        public DateTime SendDate { get; set; }

        public TaskCommentModel() : base()
        {

        }

        public TaskCommentModel(ITaskComment comment) : base(comment)
        {
            this.Id = comment.Id;
            this.CreatorId = comment.CreatorId;
            this.SendDate = comment.SendDate;
        }
    }
}
