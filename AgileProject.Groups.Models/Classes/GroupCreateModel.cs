﻿using AgileProject.Groups.Models.Transport.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class GroupCreateModel : IGroupCreate
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public GroupCreateModel()
        {

        }

        public GroupCreateModel(IGroupCreate group)
        {
            this.Description = group.Description;
            this.Name = group.Name;
        }
    }
}
