﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class MessageModel
    {
        public string Text { get; set; }
    }
}
