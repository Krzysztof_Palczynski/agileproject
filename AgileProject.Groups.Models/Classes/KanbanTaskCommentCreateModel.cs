﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class KanbanTaskCommentCreateModel : ITaskCommentCreate
    {
        public long TaskId { get; set; }

        public string Text { get; set; }

        public KanbanTaskCommentCreateModel()
        {

        }

        public KanbanTaskCommentCreateModel(ITaskCommentCreate comment)
        {
            this.TaskId = comment.TaskId;
            this.Text = comment.Text;
        }
    }
}
