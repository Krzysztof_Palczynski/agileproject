﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Classes
{
    public class GroupModel : GroupCreateModel, IGroup
    {
        public long Id { get; set; }

        public long CreatorId { get; set; }

        public GroupModel() : base()
        {

        }

        public GroupModel(IGroup group) : base(group)
        {
            this.Id = group.Id;
            this.CreatorId = group.CreatorId;
        }
    }
}
