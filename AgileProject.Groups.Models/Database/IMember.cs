﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IMember
    {
        long GroupId { get; }
        long UserId { get; }
    }
}
