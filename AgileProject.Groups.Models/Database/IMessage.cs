﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IMessage
    {
        long Id { get; }
        string Text { get; }
        long SenderId { get; }
        DateTime SendDate { get; }
        long GroupId { get; set; }
    }

}
