﻿using AgileProject.Groups.Models.Transport.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IGroup : IGroupCreate
    {
        long Id { get; }
        long CreatorId { get; }

    }
}
