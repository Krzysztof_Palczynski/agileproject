﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IKanbanTask : IKanbanTaskCreate
    {
        long Id { get; }
        long CreatorId { get; }
    }

    public interface IKanbanTaskCreate
    {
        long GroupId { get; }
        long PerformerId { get; }
        string Title { get; }
        string Description { get; }
        TaskState State { get; }
    }

    public enum TaskState
    {
        ToBeDone = 0,
        InProgress = 1,
        ToBeTested = 2,
        WaitingForApproval = 3,
        Finished = 4
    }
}
