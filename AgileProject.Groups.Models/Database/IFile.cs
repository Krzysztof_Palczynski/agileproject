﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IFile : IFileModel
    {
        string NameOnServer { get; }
        
    }

    public interface IFileModel
    {
        long Id { get; }
        long GroupId { get; }
        string NameForUsers { get; }
        long CreatorId { get; }
    }
}
