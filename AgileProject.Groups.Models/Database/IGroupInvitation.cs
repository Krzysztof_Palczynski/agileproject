﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface IGroupInvitation
    {
        long UserId { get; }
        long GroupId { get; }
        DateTime InvitationSent { get; }
    }
}
