﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Database
{
    public interface ITaskComment : ITaskCommentCreate
    {
        long Id { get; }
        long CreatorId { get; }
        DateTime SendDate { get; }
    }

    public interface ITaskCommentCreate
    {
        long TaskId { get; }
        string Text { get; }
    }
}
