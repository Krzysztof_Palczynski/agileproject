﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Models.Transport.Interfaces
{
    public interface IGroupCreate
    {
        string Name { get; }
        string Description { get; }
    }
}
