﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AgileProject.Groups.Models.Transport.Classes
{
    public class PostFileModel
    {
        public string Base64Content { get; set; }
        public string FileName { get; set; }

        public PostFileModel()
        {

        }

        public PostFileModel(byte[] content, string fileName)
        {
            SetContent(content);
            this.FileName = fileName;
        }

        public void SetContent(byte[] content)
        {
            this.Base64Content = Convert.ToBase64String(content);
        }

        public byte[] GetBytes()
        {
            return Convert.FromBase64String(Base64Content);
        }

        public string GetExtention()
        {
            return Path.GetExtension(FileName);
        }
    }
}
