﻿using System;
using System.Runtime.Serialization;
using RestSharp;

namespace AgileProject.Users.Client
{
    [Serializable]
    internal class RestTransportException : Exception
    {
        private IRestResponse<object> response;

        public RestTransportException()
        {
        }

        public RestTransportException(IRestResponse<object> response)
        {
            this.response = response;
        }

        public RestTransportException(string message) : base(message)
        {
        }

        public RestTransportException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RestTransportException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}