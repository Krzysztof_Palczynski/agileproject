﻿using AgileProject.Users.Models.Classes;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AgileProject.Users.Client
{
    public class UsersClient
    {
        string token;
        RestClient client;

        public UsersClient()
        {
            client = new RestClient("https://localhost:44305");
        }

        public bool CreateUser(CreateUserModel model)
        {
            return SendRequest<bool>("authorization/createuser", Method.POST, null, model);
        }

        public TokenResponse Login(UserLoginModel model)
        {
            var t = SendRequest<TokenResponse>("authorization/login", Method.POST, null, model);
            if (t?.JwtToken != null)
                token = t.JwtToken;

            return t;
        }

        public bool DeleteUser()
        {
            return SendRequestAuthorized<bool>("users/management/deleteuser", Method.DELETE);
        }

        public UserInfo GetUserInfo()
        {
            return SendRequestAuthorized<UserInfo>("users/management/getuserinfo", Method.GET);
        }

        public bool ChangePassword(UserChangeLoginModel model)
        {
            var result = SendRequestAuthorized<object>("users/management/changepassword", Method.PUT, null, model);
            return true;
        }

        public bool UpdateUserInfo(ChangeUserInfoModel model)
        {
            SendRequestAuthorized<object>("users/management/updateuserinfo", Method.PUT, null, model);
            return true;
        }

        protected T SendRequest<T>(string url, Method m, Action<RestRequest> addParametersFunction = null, object tosend = null) where T : new()
        {
            var request = new RestRequest(url, m);

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            if (addParametersFunction != null)
                addParametersFunction(request);
            if (tosend != null)
                request.AddJsonBody(tosend);
            var response = client.Execute<T>(request);
            if (check_if_error(response))
            {
                if(response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                throw new RestTransportException(response.Content);
            }
            return response.Data;
        }
        protected T SendRequestAuthorized<T>(string url, Method m, Action<RestRequest> addParametersFunction = null, object tosend = null) where T : new()
        {
            var result = SendRequest<T>(url, m, (r) => {
                r.AddHeader("Authorizaiton", $"Bearer {token}");
                addParametersFunction?.Invoke(r);
            }, tosend);
            return result;
        }

        private bool check_if_error<T>(IRestResponse<T> response) where T : new()
        {
            var digit = (int)response.StatusCode / 100;
            return digit == 4 || digit == 5;
        }
    }
}
