﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AgileProject.Users.Logic.Security
{
    public static class Hashing
    {
        public static string HashPassword(string password, string salt)
        {
            var hash = HashPassword(
                Convert.FromBase64String(password),
                Convert.FromBase64String(salt)
            );
            return Convert.ToBase64String(hash);
        }
        public static string HashPassword(string password, byte[] salt)
        {
            var hash = HashPassword(
                Encoding.UTF8.GetBytes(password),
                salt
            );
            return Convert.ToBase64String(hash);
        }
        public static byte[] HashPassword(byte[] password, byte[] salt)
        {
            byte[] saltedValue = password.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }
        public static byte[] GenerateSalt()
        {
            byte[] salt = new byte[16];
            new Random().NextBytes(salt);
            return salt;
        }
    }
}
