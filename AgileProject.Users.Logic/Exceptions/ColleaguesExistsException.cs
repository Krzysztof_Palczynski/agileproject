﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class ColleaguesExistsException : Exception
    {
        public ColleaguesExistsException(long user1Id, long user2Id) : base($"Users [{user1Id}, {user2Id}] are already colleagues")
        {

        }
    }
}
