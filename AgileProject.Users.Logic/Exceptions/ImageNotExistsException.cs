﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class ImageNotExistsException : Exception
    {
        public ImageNotExistsException(long userId, string pathToImage) : base($"Image of user of Id: {userId} doesn't exist under path: {pathToImage}")
        {
        }

        public ImageNotExistsException(long userId) : base($"Image of user of Id: {userId} doesn't exist") { }
    }
}
