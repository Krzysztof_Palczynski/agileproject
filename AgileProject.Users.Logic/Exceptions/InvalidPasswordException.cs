﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class InvalidPasswordException : Exception
    {
        public InvalidPasswordException() : base("Password must contain 8 letters and white spaces are not allowed")
        {
        }
    }
}
