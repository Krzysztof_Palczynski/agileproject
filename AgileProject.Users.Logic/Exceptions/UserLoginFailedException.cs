﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class UserLoginFailedException : Exception
    {
        public UserLoginFailedException(string userMail) : base($"User of email: {userMail} was not authorized")
        {
        }
    }
}
