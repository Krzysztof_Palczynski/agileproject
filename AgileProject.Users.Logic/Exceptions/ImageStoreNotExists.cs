﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class ImageStoreNotExists : Exception
    {
        public ImageStoreNotExists(string path) : base($"Image store has not been initialized under path {path}")
        {
        }
    }
}
