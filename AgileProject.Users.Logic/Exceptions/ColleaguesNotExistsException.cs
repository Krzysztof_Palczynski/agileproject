﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class ColleaguesNotExistsException : Exception
    {
        public ColleaguesNotExistsException(long user1Id, long user2Id) : base($"Users [{user1Id}, {user2Id}] are not colleagues")
        {

        }
    }
}
