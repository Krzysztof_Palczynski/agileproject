﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class UserExistsException : Exception
    {
        public UserExistsException() : base()
        {
        }
    }
}
