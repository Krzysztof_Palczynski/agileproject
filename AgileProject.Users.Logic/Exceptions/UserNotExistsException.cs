﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Logic.Exceptions
{
    public class UserNotExistsException : Exception
    {
        public UserNotExistsException(long userId) : base($"User of id: {userId} does not exist")
        {
        }
    }
}
