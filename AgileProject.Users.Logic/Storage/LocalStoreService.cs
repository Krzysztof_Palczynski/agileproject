﻿using AgileProject.Users.Logic.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AgileProject.Users.Logic.Storage
{
    public interface ILocalStoreService
    {
        byte[] GetUserImage(long userId);
        void SaveUserImage(long userId, byte[] image, string extention);
        void DeleteUserImage(long userId);
    }

    public class LocalStoreService : ILocalStoreService
    {
        string pathToRoot;
        string pathToImages => Path.Combine(pathToRoot, "images");

        public LocalStoreService(string pathToRoot)
        {
            this.pathToRoot = pathToRoot ?? throw new ArgumentNullException(nameof(pathToRoot));
        }

        /// <param name="extention">Extention of the image. String without dot at the beginning</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public void SaveUserImage(long userId, byte[] image, string extention)
        {
            if (userId < 1)
                throw new ArgumentException($"UserId is equal to {userId}");
            if (image == null)
                throw new ArgumentNullException("image is null");
            if (string.IsNullOrEmpty(extention))
                throw new ArgumentNullException("extention is null or empty");

            //extention must be without dot - this function strips extention from the dot
            while (extention[0] == '.')
                extention = extention.Substring(1);

            if (!Directory.Exists(pathToImages))
                Directory.CreateDirectory(pathToImages);

            string path = Path.Combine(pathToImages, $"{userId}.{extention}");

            File.WriteAllBytes(path, image);
        }

        /// <exception cref="ImageNotExistsException"></exception>
        /// <exception cref="ImageStoreNotExists"></exception>
        public byte[] GetUserImage(long userId)
        {
            if (!Directory.Exists(pathToImages))
                throw new ImageStoreNotExists(pathToImages);


            string filename = getImageFilename(userId);
            if(filename == null)
                throw new ImageNotExistsException(userId);

            string path = Path.Combine(pathToImages, filename);
            if (!File.Exists(path))
                throw new ImageNotExistsException(userId, path);

            return File.ReadAllBytes(path);
        }

        /// <exception cref="ImageNotExistsException"></exception>
        public void DeleteUserImage(long userId)
        {
            string filename = getImageFilename(userId);
            string path = Path.Combine(pathToImages, filename);
            if (!File.Exists(path))
                throw new ImageNotExistsException(userId, path);

            File.Delete(path);
        }

        private string getImageFilename(long userId)
        {
            var sid = userId.ToString();
            var filename = Directory
                .GetFiles(pathToImages)
                .FirstOrDefault(x =>

                    Path.GetFileName(x).Split('.')?
                    .First() == sid
                );
            return filename;
        }

        public void ClearImageStorage()
        {
            if (Directory.Exists(pathToImages))
            {
                Directory.GetFiles(pathToImages)
                    .ToList()
                    .ForEach(x => File.Delete(
                        Path.Combine(pathToImages, x)
                    )
                );
                Directory.Delete(pathToImages);
            }
        }
    }
}
