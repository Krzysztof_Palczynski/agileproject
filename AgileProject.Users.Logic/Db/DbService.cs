﻿using AgileProject.Users.Db;
using AgileProject.Users.Db.Entities;
using AgileProject.Users.Logic.Security;
using AgileProject.Users.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using AgileProject.Users.Logic.Storage;
using AgileProject.Users.Logic.Exceptions;

namespace AgileProject.Users.Logic.Db
{
    

    public class DbService : IDbService
    {
        UsersContext context;

        #region user
        public DbService(UsersContext context)
        {
            this.context = context;
        }

        /// <exception cref="InvalidEmailException"></exception>
        /// <exception cref="InvalidPasswordException"></exception>
        /// <exception cref="UserExistsException">Thrown when user with passed email already exists</exception>
        public User CreateUser(ICreateDbUser user)
        {
            if (!checkEmail(user.Email))
                throw new InvalidEmailException(user.Email);
            if (!checkPassword(user.Password))
                throw new InvalidPasswordException();

            var userExists = context.Users.Any(x => x.Email == user.Email);
            if (userExists)
                throw new UserExistsException();

            var salt = Hashing.GenerateSalt();
            var hash = Hashing.HashPassword(user.Password, salt);

            var dbUser = new User()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PasswordHash = hash,
                PasswordSalt = Convert.ToBase64String(salt)
            };

            context.Entry(dbUser).State = EntityState.Added;
            context.SaveChanges();
            context.Entry(dbUser).GetDatabaseValues();

            return dbUser;
        }
        /// <returns>If there is no such user method will return null</returns>
        public User ChallengeUser(IUserLogin login)
        {
            var user = context.Users.FirstOrDefault(x => x.Email == login.Email);
            if (user == null)
                return null;

            var saltBytes = Convert.FromBase64String(user.PasswordSalt);

            var hash = Hashing.HashPassword(login.Password, saltBytes);
            if (user.PasswordHash == hash)
                return user;
            else
                return null;
        }

        /// <exception cref="UserLoginFailedException"></exception>
        /// <exception cref="InvalidPasswordException"></exception>
        public User ChangePassword(IUserLogin login, string newPassword)
        {
            if (!checkPassword(newPassword))
                throw new InvalidPasswordException();

            var user = ChallengeUser(login);
            if (user == null)
                throw new UserLoginFailedException(login.Email);

            byte[] newSalt = Hashing.GenerateSalt();
            string newHash = Hashing.HashPassword(newPassword, newSalt);

            user.PasswordHash = newHash;
            user.PasswordSalt = Convert.ToBase64String(newSalt);

            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();

            return user;
        }

        /// <exception cref="UserLoginFailedException"></exception>
        public User ChangePassword(IUserChangeLogin login)
        {
            return ChangePassword(login, login.NewPassword);
        }

        /// <exception cref="InvalidEmailException"></exception>
        /// <exception cref="UserNotExistsException"></exception>
        public User ChangeUserInfo(long userId, IChangeUserDbInfo info)
        {
            if (!checkEmail(info.Email))
                throw new InvalidEmailException(info.Email);

            var user = context.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
                throw new UserNotExistsException(userId);

            user.Email = info.Email;
            user.FirstName = info.FirstName;
            user.LastName = info.LastName;

            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();

            return user;
        }

        /// <exception cref="UserNotExistsException"></exception>
        public bool DeleteUser(long userId)
        {
            var user = context.Users.FirstOrDefault(x => x.Id == userId);
            if (user == null)
                throw new UserNotExistsException(userId);
            context.Entry(user).State = EntityState.Deleted;
            context.SaveChanges();
            return true;
        }
        /// <exception cref="UserNotExistsException"></exception>
        public User GetUserInfo(long userId)
        {
            var user = context.Users.Include(x => x.Colleagues).FirstOrDefault(x => x.Id == userId);
            if (user == null)
                throw new UserNotExistsException(userId);
            return user;
        }
        public List<User> GetUsers()
        {
            return context.Users.ToList();
        }
        #endregion

        #region colleagues
        /// <exception cref="UserNotExistsException"></exception>
        /// <exception cref="ColleaguesExistsException"></exception>
        public void AddColleague(long userId, long colleagueId)
        {
            if (areColleaguesExist(userId, colleagueId, out var c))
                throw new ColleaguesExistsException(userId, colleagueId);

            var cc = new Colleague()
            {
                Colleage1Id = userId,
                Colleague2Id = colleagueId
            };

            context.Entry(cc).State = EntityState.Added;
            context.SaveChanges();
        }

        /// <exception cref="UserNotExistsException"></exception>
        private bool areColleaguesExist(long userId, long colleagueId, out Colleague colleague)
        {
            var user = GetUserInfo(userId);
            var col = GetUserInfo(colleagueId);

            colleague = context.Colleagues.
                FirstOrDefault(x => x.AreColleagues(userId, colleagueId));

            return colleague != null;
        }

        public void RemoveColleague(long userId, long colleagueId)
        {
            if (!areColleaguesExist(userId, colleagueId, out var colleague))
                throw new ColleaguesNotExistsException(userId, colleagueId);

            context.Entry(colleague).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public List<long> GetUserColleagues(long userId)
        {
            var cc = context.Colleagues.Where(x => x.IsUserAColleague(userId));
            var toreturn = cc.Select(x => x.Colleage1Id)
                .Concat(cc.Select(x=>x.Colleague2Id))
                .Distinct()
                .Where(x=>x != userId)
                .ToList();

            return toreturn;
        }
        #endregion

        #region helpers
        public bool checkEmail(string email)
        {
            if (!email.Contains('@'))
                return false;
            if (email.Split('@').Length != 2)
                return false;
            if (!email.Split('@')[1].Contains('.'))
                return false;
            if (email.Contains(' '))
                return false;
            return true;
        }
        public bool checkPassword(string password)
        {
            if (password.Length < 8)
                return false;
            if (password.Contains(' '))
                return false;
            return true;
        }
        #endregion
    }

    public interface IDbService : IDbUserAuthorizationProvider, IDbUserManagementProvider
    {
        User ChangePassword(IUserLogin login, string newPassword);
        List<User> GetUsers();
    }

    public interface IDbUserAuthorizationProvider
    {
        User CreateUser(ICreateDbUser user);
        User ChallengeUser(IUserLogin login);
    }

    public interface IDbUserManagementProvider
    {
        bool DeleteUser(long userId);
        User GetUserInfo(long userId);
        User ChangePassword(IUserChangeLogin login);
        User ChangeUserInfo(long userId, IChangeUserDbInfo info);
    }
}
