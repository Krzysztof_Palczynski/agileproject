﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AgileProject.Groups.Logic.LocalStorage
{
    public class FilesStore
    {
        string pathToRoot;
        const string store_directory_name = "localstore";

        public FilesStore(string pathToRoot)
        {
            this.pathToRoot = pathToRoot;
        }

        /// <param name="extention">Without dot</param>
        /// <returns>Returns name of new file</returns>
        public string CreateFile(byte[] bytes, long groupId, string extention)
        {
            if (!Directory.Exists(pathToLocalStore))
                Directory.CreateDirectory(pathToLocalStore);

            string pathToFolder = pathToGroupFolder(groupId);
            if (!Directory.Exists(pathToFolder))
                Directory.CreateDirectory(pathToFolder);

            extention = extentionWithoutDot(extention);
            string name = $"{Guid.NewGuid().ToString()}.{extention}";
            string path = Path.Combine(pathToFolder, name);

            File.WriteAllBytes(path, bytes);

            return name;
        }

        /// <exception cref="DirectoryNotFoundException"></exception>
        /// <exception cref="FileNotFoundException"></exception>
        public void DeleteFile(string filename, long groupId)
        {
            var path = checkIfFileExists(filename, groupId);

            File.Delete(path);
        }

        /// <exception cref="DirectoryNotFoundException"></exception>
        /// <exception cref="FileNotFoundException"></exception>
        public byte[] DownloadFile(string filename, long groupId)
        {
            var path = checkIfFileExists(filename, groupId);

            return File.ReadAllBytes(path);
        }

        public void DeleteStore()
        {
            Directory.Delete(pathToLocalStore, true);
        }



        private string pathToLocalStore => Path.Combine(pathToRoot, store_directory_name);
        private string pathToGroupFolder(long groupId)
        {
            return Path.Combine(pathToRoot, store_directory_name, $"group_{groupId}");
        }

        /// <summary>
        /// WARNING! This method throws exceptions instead of returning bool values
        /// </summary>
        /// <exception cref="DirectoryNotFoundException"></exception>
        /// <exception cref="FileNotFoundException"></exception>
        /// <returns>Path to file</returns>
        private string checkIfFileExists(string filename, long groupId)
        {
            if (!Directory.Exists(pathToLocalStore))
                throw new DirectoryNotFoundException("Store is not initialized");

            string pathToFolder = pathToGroupFolder(groupId);
            if (!Directory.Exists(pathToFolder))
                throw new DirectoryNotFoundException($"Group of id: {groupId} doesn't have it's folder");

            string pathToFile = Path.Combine(pathToFolder, filename);
            if (!File.Exists(pathToFile))
                throw new FileNotFoundException($"File of name: {filename} couldn't be found");

            return pathToFile;
        }

        /// <exception cref="ArgumentNullException"></exception>
        private string extentionWithoutDot(string extention)
        {
            if (extention == null)
                throw new ArgumentNullException("extention");

            while (extention.Contains("."))
                extention = extention.Substring(1);

            return extention;
        }
    }
}
