﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class UserNotRelatedToKanbanTaskException : Exception
    {
        public UserNotRelatedToKanbanTaskException(long userId, long taskId) : base($"User of id: {userId} is neither creator not performer of task: {taskId}")
        {
        }
    }
}
