﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class EntityNotExistException : Exception
    {
        public EntityNotExistException(string entity, long entityId) : base($"Entity: {entity} of Id: {entityId} does not exists in the database")
        {
        }
    }
}
