﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class InvitationNotExistException : Exception
    {
        public InvitationNotExistException(long groupId, long userId) : base($"Invitation wasn't found for user: {userId} in group: {groupId}")
        {
        }
    }
}
