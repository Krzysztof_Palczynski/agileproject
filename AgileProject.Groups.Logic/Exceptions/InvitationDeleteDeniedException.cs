﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class InvitationDeleteDeniedException : Exception
    {
        public InvitationDeleteDeniedException(long removerId) : base($"Attempt to remove invitation by user of Id: {removerId}. Invitation can be removed only by creator of group or invited person")
        {
        }
    }
}
