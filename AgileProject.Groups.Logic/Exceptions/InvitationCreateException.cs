﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class InvitationCreatePermissionsException : Exception
    {
        public InvitationCreatePermissionsException(long userId) : base($"Attempt to create invitation by user of Id: {userId}. Invitations can be created only by creators of the group")
        {
        }
    }
}
