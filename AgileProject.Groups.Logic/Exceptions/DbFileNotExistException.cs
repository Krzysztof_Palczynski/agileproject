﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class DbFileNotExistException : Exception
    {
        public DbFileNotExistException(long fileId) : base($"File of id: {fileId} does not exist in database")
        {
        }
    }
}
