﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class KanbanTaskNotExistsException : Exception
    {
        public KanbanTaskNotExistsException(long taskId) : base($"Kanban task of id: {taskId} does not exists in the database")
        {
        }
    }
}
