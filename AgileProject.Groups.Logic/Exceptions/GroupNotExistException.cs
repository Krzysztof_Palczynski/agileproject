﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class GroupNotExistException : EntityNotExistException
    {
        public GroupNotExistException(long groupId) : base("group", groupId)
        {
        }
    }
}
