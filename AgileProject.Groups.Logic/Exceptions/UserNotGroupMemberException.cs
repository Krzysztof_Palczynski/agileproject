﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class UserNotGroupMemberException : Exception
    {
        public UserNotGroupMemberException(long userId, long groupId) : base($"User of Id: {userId} is not member of group: {groupId}")
        {
        }
    }
}
