﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class MemberNotExistException : EntityNotExistException
    {
        public MemberNotExistException(long userId) : base("Member", userId)
        {
        }
    }
}
