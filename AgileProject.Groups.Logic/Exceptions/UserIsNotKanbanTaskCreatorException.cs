﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class UserIsNotKanbanTaskCreatorException : Exception
    {
        public UserIsNotKanbanTaskCreatorException(long userId, long taskId) : base($"User of id: {userId} is not creator of task {taskId}")
        {
        }
    }
}
