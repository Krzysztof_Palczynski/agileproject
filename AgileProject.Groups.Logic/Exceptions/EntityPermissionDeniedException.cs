﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class EntityPermissionDeniedException : Exception
    {
        public EntityPermissionDeniedException(long userId, string entityName, long entityId, string action) : base($"User of Id: {userId} attemted to perform action: {action} on entity: {entityName} of id: {entityId} without required permissions")
        {
        }
    }
}
