﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class InvitationExistsException : Exception
    {
        public InvitationExistsException(long userId, long groupId) : base($"Invitation for user: {userId} to group: {groupId} already exists")
        {
        }
    }
}
