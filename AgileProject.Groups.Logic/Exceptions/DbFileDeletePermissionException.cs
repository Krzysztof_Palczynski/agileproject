﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class DbFileDeletePermissionException : Exception
    {
        public DbFileDeletePermissionException(long userId, long fileId) : base($"User of id: {userId} is not allowed to remove file of id: {fileId}. User is not creator of the file and user is not creator of the group to which this file belongs")
        {
        }
    }
}
