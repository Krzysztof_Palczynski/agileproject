﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class GroupPermissionDeniedException : EntityPermissionDeniedException
    {
        public GroupPermissionDeniedException(long userId, long entityId) : base(userId, "group", entityId, "action required creator's permission")
        {
        }
    }
}
