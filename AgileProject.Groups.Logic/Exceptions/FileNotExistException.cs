﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class FileNotExistException : Exception
    {
        public FileNotExistException(long fileId) : base($"File of id: {fileId} does not exist in the database")
        {
        }
    }
}
