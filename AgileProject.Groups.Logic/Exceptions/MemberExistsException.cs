﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class MemberExistsException : Exception
    {
        public MemberExistsException(long userId, long groupId) : base($"User: {userId} already exists in group: {groupId}")
        {
        }
    }
}
