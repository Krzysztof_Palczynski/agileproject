﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class UserIsNotGroupMemberException : Exception
    {
        public UserIsNotGroupMemberException(long userId, long groupId) : base($"User of id: {userId} is not a member of group: {groupId} or its creator")
        {
        }
    }
}
