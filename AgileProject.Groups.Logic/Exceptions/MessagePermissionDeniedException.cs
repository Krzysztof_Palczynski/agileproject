﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Logic.Exceptions
{
    public class MessagePermissionDeniedException : Exception
    {
        public MessagePermissionDeniedException(long userId, long groupId) : base($"User of id: {userId} is not allowed to create message in group: {groupId}. User is not member of the group and not a creator of the group")
        {
        }
    }
}
