﻿using AgileProject.Groups.Database.Context;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Database;
using AgileProject.Groups.Models.Transport.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileProject.Groups.Logic.Database
{
    public class DbService
    {
        private GroupsContext db;

        public DbService(GroupsContext db)
        {
            this.db = db;
        }

        

        #region groups

        /// <summary>
        /// Creates group
        /// </summary>
        /// <param name="userId">Id of user creating group. It will be added to group as CreatorId</param>
        public Group GroupCreate(long userId, IGroupCreate group)
        {
            var dbg = new Group(group, userId);
            db.Entry(dbg).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(dbg).GetDatabaseValues();
            return dbg;
        }
        /// <summary>
        /// Updates information about group. User must be creator of the group
        /// </summary>
        /// <param name="userId">Id of user that uses this function. Must be creator of the group</param>
        /// <param name="name">If null then it won't be changed</param>
        /// <param name="description">If null then it won't be changed</param>
        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        /// <exception cref="GroupPermissionDeniedException">Thrown if user of userId is not creator of the group</exception>
        /// <returns></returns>
        public Group GroupUdpateInfo(long userId, long groupId, string name = null, string description = null)
        {
            var group = getGroup(groupId, userId);

            if (name != null)
                group.Name = name;
            if (description != null)
                group.Description = description;

            db.Entry(group).State = EntityState.Modified;
            db.SaveChanges();

            return group;
        }

        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        /// <exception cref="GroupPermissionDeniedException">Thrown if user of userId is not creator of the group</exception>
        public void GroupDelete(long userId, long groupId)
        {
            var group = getGroup(groupId, userId);

            db.Entry(group).State = EntityState.Deleted;
            db.SaveChanges();
        }

        public List<Group> GroupList(long userId)
        {
            var groups = db.Groups
                .Include(x => x.Members)
                .Where(x => x.CreatorId == userId || x.Members.Any(y => y.UserId == userId))
                .GroupBy(x => x.Id)
                .Select(x => x.First())
                .ToList();

            return groups;
        }

        



        #endregion
        #region group-invitations
        /// <summary>
        /// Get invitations of particular group
        /// </summary>
        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="GroupPermissionDeniedException"></exception>
        public List<GroupInvitation> InvitationsGetToGroup(long userId, long groupId)
        {
            var group = db.Groups
                .Include(x=>x.Invitations)
                .FirstOrDefault(x => x.Id == groupId);
            if (group == null)
                throw new GroupNotExistException(groupId);
            if (group.CreatorId != userId)
                throw new GroupPermissionDeniedException(userId, groupId);

            return group.Invitations.ToList();
        }

        /// <summary>
        /// Get invitations for user to groups
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<GroupInvitation> InvitationsGetUsers(long userId)
        {
            return db.Invitations
                .Where(x => x.UserId == userId)
                .ToList();
        }

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="InvitationCreatePermissionsException"></exception>
        /// <exception cref="InvitationExistsException"></exception>
        public GroupInvitation InvitationsCreate(long userId, long invitedPersonId, long groupId)
        {
            var group = db.Groups.FirstOrDefault(x => x.Id == groupId);
            if (group == null)
                throw new GroupNotExistException(groupId);
            if (group.CreatorId != userId)
                throw new InvitationCreatePermissionsException(userId);

            if (db.Invitations.Any(x => x.UserId == invitedPersonId && x.GroupId == groupId))
                throw new InvitationExistsException(invitedPersonId, groupId);

            var i = new GroupInvitation()
            {
                GroupId = groupId,
                UserId = invitedPersonId
            };

            db.Entry(i).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(i).GetDatabaseValues();

            return i;
        }

        /// <param name="removerId">This field must be id of person invited or creator of group</param>
        /// <param name="userId">Id of user whose invitation has been removed</param>
        /// <exception cref="InvitationNotExistException"></exception>
        /// <exception cref="InvitationDeleteDeniedException"></exception>
        public void InvitationsDelete(long removerId, long groupId, long userId)
        {
            var invitation = db.Invitations
                .Include(x => x.Group)
                .FirstOrDefault(x => x.GroupId == groupId && x.UserId == userId);

            if (invitation == null)
                throw new InvitationNotExistException(groupId, userId);

            if (removerId != userId && invitation.Group.CreatorId != removerId)
                throw new InvitationDeleteDeniedException(removerId);

            db.Entry(invitation).State = EntityState.Deleted;
            db.SaveChanges();
        }

        /// <exception cref="InvitationNotExistException"></exception>
        /// <exception cref="MemberExistsException"></exception>
        public void InvitationsAccept(long userId, long groupId)
        {
            var invitaiton = db.Invitations
                .Include(x => x.Group)
                .FirstOrDefault(x => x.UserId == userId && x.GroupId == groupId);

            if (invitaiton == null)
                throw new InvitationNotExistException(groupId, userId);

            if (db.Members.Any(x => x.GroupId == groupId && x.UserId == userId))
                throw new MemberExistsException(userId, groupId);

            var member = new Member()
            {
                GroupId = groupId,
                UserId = userId
            };

            db.Entry(member).State = EntityState.Added;
            db.Entry(invitaiton).State = EntityState.Deleted;
            db.SaveChanges();
        }

        #endregion
        #region group-members

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="UserNotGroupMemberException"></exception>
        public List<Member> GroupMembersGet(long userId, long groupId)
        {
            var group = db
                .Groups
                .Include(x => x.Members)
                .FirstOrDefault(x => x.Id == groupId);

            if (group == null)
                throw new GroupNotExistException(groupId);

            if (group.CreatorId != userId && group.Members.Any(x => x.UserId == userId) == false)
                throw new UserNotGroupMemberException(userId, groupId);

            return group.Members
                .ToList();
        }

        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        /// <exception cref="GroupPermissionDeniedException">Thrown if user of userId is not creator of the group</exception>
        /// <exception cref="MemberNotExistException">Thrown if member of passed UserId and GroupId does not exists</exception>
        public void GroupMemberDelete(long userId, long groupId, long memberId)
        {
            //checking if group exists and if user is creator of the group
            getGroup(groupId, userId);

            var member = db.Members.FirstOrDefault(x => x.GroupId == groupId && x.UserId == memberId);
            if (member == null)
                throw new MemberNotExistException(memberId);

            db.Entry(member).State = EntityState.Deleted;
            db.SaveChanges();
        }

        #endregion
        #region group-files

        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        /// <exception cref="UserNotGroupMemberException">Thrown if user is not a group member or creator</exception>
        public File FileCreateEntry(long userId, long groupId, string nameForUsers, string nameOnServer = null)
        {
            if (!checkIfUserIsMemberOfGroup(userId, groupId))
                throw new UserNotGroupMemberException(userId, groupId);

            if (nameOnServer == null)
                nameOnServer = $"{Guid.NewGuid().ToString()}-{nameForUsers}";

            var file = new File()
            {
                GroupId = groupId,
                NameForUsers = nameForUsers,
                NameOnServer = nameOnServer,
                CreatorId = userId
            };

            db.Entry(file).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(file).GetDatabaseValues();

            return file;
        }

        /// <param name="userId">User must be creator of file or creator of group that this file belongs</param>
        /// <exception cref="DbFileNotExistException"></exception>
        /// <exception cref="DbFileDeletePermissionException"></exception>
        public void FileDeleteEntry(long userId, long fileId)
        {
            var file = db.Files
                .Include(x => x.Group)
                .FirstOrDefault(x => x.Id == fileId);

            if (file == null)
                throw new DbFileNotExistException(fileId);
            if (file.CreatorId != userId && file.Group.CreatorId != userId)
                throw new DbFileDeletePermissionException(userId, fileId);

            db.Entry(file).State = EntityState.Deleted;
            db.SaveChanges();
        }

        /// <exception cref="FileNotExistException"></exception>
        /// <exception cref="UserNotGroupMemberException"></exception>
        public File FileGet(long userId, long fileId)
        {
            var file = db
                .Files
                .Include(x => x.Group)
                .ThenInclude(x=>x.Members)
                .FirstOrDefault(x => x.Id == fileId);

            if (file == null)
                throw new FileNotExistException(fileId);

            if (file.Group.CreatorId != userId && file.Group.Members.Any(x => x.UserId == userId) == false)
                throw new UserNotGroupMemberException(userId, file.GroupId);

            return file;
        }

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="GroupPermissionDeniedException"></exception>
        /// <returns></returns>
        public List<File> FilesList(long userId, long groupId)
        {
            var group = getGroup(groupId, userId);
            return group.Files?.ToList();
        }
        #endregion
        #region group-messages

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="MessagePermissionDeniedException"></exception>
        public Message MessageCreate(long userId, long groupId, string text)
        {
            var group = getGroup(groupId);
            if (group.CreatorId != userId && !group.Members.Any(x => x.UserId == userId))
                throw new MessagePermissionDeniedException(userId, groupId);

            var message = new Message()
            {
                SenderId = userId,
                Text = text,
                GroupId = groupId
            };

            db.Entry(message).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(message).GetDatabaseValues();

            return message;
        }

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="MessagePermissionDeniedException"></exception>
        public List<Message> MessagesList(long userId, long groupId)
        {
            var group = getGroup(groupId);
            if (group.CreatorId != userId && !group.Members.Any(x => x.UserId == userId))
                throw new MessagePermissionDeniedException(userId, groupId);

            return group.Messages?.ToList();
        }

        #endregion
        #region kanban

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="UserIsNotGroupMemberException"></exception>
        public KanbanTask KanbanTaskCreate(long userId, long groupId, long performerId, string title, string description)
        {
            if (!checkIfUserIsMemberOfGroup(userId, groupId))
                throw new UserIsNotGroupMemberException(userId, groupId);
            if (!checkIfUserIsMemberOfGroup(performerId, groupId))
                throw new UserIsNotGroupMemberException(performerId, groupId);

            var task = new KanbanTask()
            {
                CreatorId = userId,
                GroupId = groupId,
                Description = description,
                Title = title,
                PerformerId = performerId,
                State = TaskState.ToBeDone
            };

            db.Entry(task).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(task).GetDatabaseValues();

            return task;
        }

        /// <exception cref="KanbanTaskNotExistsException"></exception>
        /// <exception cref="UserNotRelatedToKanbanTaskException"></exception>
        public void KanbanTaskChangeState(long userId, long taskId, TaskState state)
        {
            var task = db.KanbanTasks
                .Include(x => x.Group)
                .FirstOrDefault(x => x.Id == taskId);

            if (task == null)
                throw new KanbanTaskNotExistsException(taskId);
            if (task.CreatorId != userId && task.PerformerId != userId)
                throw new UserNotRelatedToKanbanTaskException(userId, taskId);

            task.State = state;
            db.Entry(task).State = EntityState.Modified;
            db.SaveChanges();
        }

        /// <exception cref="KanbanTaskNotExistsException"></exception>
        /// <exception cref="UserIsNotKanbanTaskCreatorException"></exception>
        public void KanbanTaskDelete(long userId, long taskId)
        {
            var task = db.KanbanTasks.FirstOrDefault(x => x.Id == taskId);
            if (task == null)
                throw new KanbanTaskNotExistsException(taskId);
            if (task.CreatorId != userId)
                throw new UserIsNotKanbanTaskCreatorException(userId, taskId);

            db.Entry(task).State = EntityState.Deleted;
            db.SaveChanges();
        }

        /// <exception cref="GroupNotExistException"></exception>
        /// <exception cref="UserNotGroupMemberException"></exception>
        public List<KanbanTask> KanbanTasksList(long userId, long groupId)
        {
            var group = db.Groups
                .Include(x => x.Tasks)
                .Include(x=>x.Members)
                .FirstOrDefault(x => x.Id == groupId);

            if (group == null)
                throw new GroupNotExistException(groupId);

            if (group.CreatorId != userId && group.Members.Any(x => x.UserId == userId) == false)
                throw new UserNotGroupMemberException(userId, groupId);

            return group.Tasks.ToList();
        }

        #endregion

        #region kanbantask-commemnts

        /// <exception cref="KanbanTaskNotExistsException"></exception>
        /// <exception cref="UserNotGroupMemberException"></exception>
        public TaskComment KanbanTaskCommentCreate(long userId, long taskId, string text)
        {
            var task = db.KanbanTasks
                .Include(x=>x.Group)
                .Include(x=>x.Group.Members)
                .FirstOrDefault(x => x.Id == taskId);
            if (task == null)
                throw new KanbanTaskNotExistsException(taskId);
            if (task.Group.CreatorId != userId && !task.Group.Members.Any(x => x.UserId == userId))
                throw new UserNotGroupMemberException(userId, task.Group.Id);

            var comment = new TaskComment()
            {
                CreatorId = userId,
                TaskId = taskId,
                Text = text
            };

            db.Entry(comment).State = EntityState.Added;
            db.SaveChanges();
            db.Entry(comment).GetDatabaseValues();

            return comment;
        }

        /// <exception cref="KanbanTaskNotExistsException"></exception>
        /// <exception cref="UserNotGroupMemberException"></exception>
        public List<TaskComment> KanbanTaskCommentsList(long userId, long taskId)
        {
            var task = db.KanbanTasks
                .Include(x => x.Group)
                .Include(x => x.Group.Members)
                .Include(x=>x.Comments)
                .FirstOrDefault(x => x.Id == taskId);
            if (task == null)
                throw new KanbanTaskNotExistsException(taskId);
            if (task.Group.CreatorId != userId && !task.Group.Members.Any(x => x.UserId == userId))
                throw new UserNotGroupMemberException(userId, task.Group.Id);

            return task.Comments.ToList();
        }

        #endregion

        #region helpers

        ///<param name="userId">If userId > 0 then method will perfrom check, if user is creator of group</param>
        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        /// <exception cref="GroupPermissionDeniedException">Thrown if user of userId is not creator of the group</exception>
        private Group getGroup(long groupId, long userId = 0)
        {
            var group = db.Groups
                .Include(x=>x.Files)
                .Include(x=>x.Members)
                .Include(x=>x.Messages)
                .FirstOrDefault(x => x.Id == groupId);
            if (group == null)
                throw new GroupNotExistException(groupId);
            if(userId > 0 && group.CreatorId != userId)
                throw new GroupPermissionDeniedException(userId, groupId);
            return group;
        }
        /// <exception cref="GroupNotExistException">Thrown if there is no group of passed Id in the database</exception>
        private bool checkIfUserIsMemberOfGroup(long userId, long groupId)
        {
            var group = getGroup(groupId);
            if (group.CreatorId == userId)
                return true;

            return group.Members.Any(x => x.UserId == userId);
        }
        #endregion
    }
}
