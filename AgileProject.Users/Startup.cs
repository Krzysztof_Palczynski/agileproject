﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AgileProject.Users.Db;
using AgileProject.Users.Logic.Db;
using AgileProject.Users.Logic.Storage;
using AgileProject.Users.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AgileProject.Users
{
    public class Startup
    {
        public const string DatabaseConnection = @"Server=(localdb)\mssqllocaldb;Database=AgileProject.Users.Db;Trusted_Connection=True;ConnectRetryCount=0";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region database-configuration
            services.AddDbContext<UsersContext>(options => {
                options.UseSqlServer(DatabaseConnection,
                    sqlServerOptionsAction: sqlOptions => {
                        sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 5,
                            maxRetryDelay: TimeSpan.FromSeconds(30),
                            errorNumbersToAdd: null
                        );
                    }
                );
            });
            #endregion

            #region jwt-configuration
            var parameters = new JwtTokenService();
            services.AddAuthentication(x => {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, x => {
                x.RequireHttpsMetadata = false;
                x.TokenValidationParameters = parameters.CreateTokenValidationParameters();
                x.Challenge = "some challenge";
            });
            #endregion

            #region swashbuckle
            services.AddSwaggerGen(options => {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info()
                {
                    Title = "Swagger title",
                    Version = "v1"
                });
            });
            #endregion

            var sp = services.BuildServiceProvider();

            #region singletons

            #region db-service
            var context = sp.GetService<UsersContext>();
            var dbService = new DbService(context);
            services.AddSingleton(dbService);
            services.AddSingleton<IDbUserAuthorizationProvider>(x => x.GetRequiredService<IDbService>());
            services.AddSingleton<IDbUserManagementProvider>(x => x.GetRequiredService<IDbService>());
            #endregion

            #region local-storage
            var env = sp.GetService<IHostingEnvironment>();
            var localstorage = new LocalStoreService(env.ContentRootPath);
            services.AddSingleton(localstorage);
            #endregion

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            #region swashbuckle
            app.UseSwagger().UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "AgileProject.Users API v1");
            });
            #endregion
            #region authorization
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            #endregion

            app.UseMvc();
        }
    }
}
