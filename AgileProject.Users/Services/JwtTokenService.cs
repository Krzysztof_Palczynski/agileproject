﻿using AgileProject.Users.Logic.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgileProject.Users.Services
{
    public class JwtTokenService : JwtService
    {
        protected const string _JwtSecret = "secretsecretsecretsecret";

        protected const string _JwtIssuer = "issuer";

        protected const string _JwtAudienceId = "audienceid";

        public JwtTokenService() : base(_JwtSecret, _JwtIssuer, _JwtAudienceId, TimeSpan.FromDays(1))
        {
        }
    }
}
