﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Users.Helpers;
using AgileProject.Users.Logic.Exceptions;
using AgileProject.Users.Logic.Storage;
using AgileProject.Users.Models.Classes.Transport;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Users.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ImagesController : ControllerBase
    {
        long userId => this.GetUserId();
        LocalStoreService store;

        public ImagesController(LocalStoreService store)
        {
            this.store = store;
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(FileTransport), 200)]
        [ProducesResponseType(typeof(string), 401)]
        [ProducesResponseType(404)]
        public IActionResult GetUserImage(long userId)
        {
            try
            {
                var image = store.GetUserImage(userId);
                if (image == null)
                    return NotFound();

                var file = new FileTransport(image);
                return Ok(file);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult PostUserImage([FromBody]FileTransport file)
        {
            try
            {
                var bytes = file.GetByteArray();
                store.SaveUserImage(userId, bytes, file.GetExtention());
                return Ok();
            }
            catch(NullReferenceException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult DeleteUserImage()
        {
            try
            {
                store.DeleteUserImage(userId);
                return Ok();
            }
            catch(ImageNotExistsException)
            {
                return NotFound();
            }
        }
    }
}