﻿using AgileProject.Users.Db;
using AgileProject.Users.Logic.Db;
using AgileProject.Users.Logic.Exceptions;
using AgileProject.Users.Models.Classes;
using AgileProject.Users.Services;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Users.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        DbService db;

        public AuthorizationController(DbService db)
        {
            this.db = db;
        }

        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string),400)]
        [HttpPost]
        [Route("[action]")]
        public IActionResult CreateUser([FromBody]CreateUserModel user)
        {
            try
            {
                var result = db.CreateUser(user);
                return Ok(true);
            }
            catch(InvalidEmailException e)
            {
                return BadRequest(e.Message);
            }
            catch(InvalidPasswordException e)
            {
                return BadRequest(e.Message);
            }
            catch(UserExistsException)
            {
                return BadRequest("User with this email already exists");
            }
        }

        [ProducesResponseType(typeof(TokenResponse), 200)]
        [ProducesResponseType(401)]
        [HttpPost]
        [Route("[action]")]
        public IActionResult Login([FromBody] UserLoginModel user)
        {
            var result = db.ChallengeUser(user);
            if (result == null)
                return Unauthorized();

            JwtTokenService service = new JwtTokenService();
            var token = service.GetJWToken(result.Id);

            return Ok(new TokenResponse() {JwtToken = token });
        }
    }
}
