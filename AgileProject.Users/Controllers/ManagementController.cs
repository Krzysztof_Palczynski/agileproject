﻿using AgileProject.Users.Db;
using AgileProject.Users.Db.Entities;
using AgileProject.Users.Helpers;
using AgileProject.Users.Logic.Db;
using AgileProject.Users.Logic.Exceptions;
using AgileProject.Users.Logic.Storage;
using AgileProject.Users.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgileProject.Users.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class ManagementController : ControllerBase
    {
        long userId
        {
            get => this.GetUserId();
        }
        DbService db;

        public ManagementController(DbService db)
        {
            this.db = db;
        }
        

        [HttpDelete]
        [Route("[action]")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        public IActionResult DeleteUser()
        {
            var id = userId;
            try
            {
                var result = db.DeleteUser(id);
                return Ok(result);
            }
            catch (UserNotExistsException) { return NotFound(); }
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(UserInfo), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetUserInfo()
        {
            try
            {
                var result = db.GetUserInfo(userId);
                var model = new UserInfo(result);
                return Ok(model);
            }
            catch(UserNotExistsException)
            {
                return NotFound();
            }
        }

        [HttpPut]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        public IActionResult ChangePassword([FromBody]UserChangeLoginModel model)
        {
            try
            {
                var result = db.ChangePassword(model);
                return Ok();
            }
            catch (UserLoginFailedException) { return Unauthorized(); }
        }

        [HttpPut]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult UpdateUserInfo(ChangeUserInfoModel model)
        {
            try
            {
                var result = db.ChangeUserInfo(userId, model);
                return Ok();
            }
            catch (UserNotExistsException) { return NotFound(); }
        }

    }
}
