﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class GroupInvitation : IGroupInvitation
    {
        [Key]
        public long UserId { get; set; }
        [Key, ForeignKey(nameof(Group))]
        public long GroupId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime InvitationSent { get; set; }

        //navigation
        public Group Group { get; set; }
    }
}
