﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class File : IFile
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey(nameof(Group))]
        public long GroupId { get; set; }
        [Required]
        public long CreatorId { get; set; }
        [Required]
        public string NameForUsers { get; set; }
        [Required]
        public string NameOnServer { get; set; }

        //navigation
        public virtual Group Group { get; set; }
    }
}
