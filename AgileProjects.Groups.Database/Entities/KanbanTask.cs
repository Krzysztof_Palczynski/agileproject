﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class KanbanTask : IKanbanTask
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey(nameof(Group))]
        public long GroupId { get; set; }
        [Required]
        public long CreatorId { get; set; }
        [Required]
        public long PerformerId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public TaskState State { get; set; }

        //navigation
        public virtual Group Group { get; set; }
        public virtual ICollection<TaskComment> Comments { get; set; }
    }
}
