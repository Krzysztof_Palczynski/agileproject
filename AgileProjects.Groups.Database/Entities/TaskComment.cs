﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class TaskComment : ITaskComment
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey(nameof(Task))]
        public long TaskId { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public long CreatorId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime SendDate { get; set; }

        //navigation
        public virtual KanbanTask Task { get; set; }
    }
}
