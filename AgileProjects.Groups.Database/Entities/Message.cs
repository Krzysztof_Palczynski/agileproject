﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class Message : IMessage
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public long SenderId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime SendDate { get; set; }
        public long GroupId { get; set; }
    }
}
