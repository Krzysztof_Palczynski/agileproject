﻿using AgileProject.Groups.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class Member : IMember
    {
        [Key]
        [ForeignKey(nameof(Group))]
        public long GroupId { get; set; }
        [Key]
        public long UserId { get; set; }

        //navigation
        public virtual Group Group { get; set; }
    }
}
