﻿using AgileProject.Groups.Models.Database;
using AgileProject.Groups.Models.Transport.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Groups.Database.Entities
{
    public class Group : IGroup
    {
        public Group() { }
        public Group(IGroupCreate group, long creatorId)
        {
            this.Name = group.Name;
            this.Description = group.Description;
            this.CreatorId = creatorId;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public long CreatorId { get; set; }

        //navigation
        public virtual ICollection<File> Files { get; set; }
        public virtual ICollection<KanbanTask> Tasks { get; set; }
        public virtual ICollection<Member> Members { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<GroupInvitation> Invitations { get; set; }
    }
}
