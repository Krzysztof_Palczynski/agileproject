﻿using AgileProject.Groups.Database.Entities;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Groups.Database.Context
{
    public class GroupsContext : DbContext
    {
        public GroupsContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Member - stworzenie klucza kompozytowego
            modelBuilder.Entity<Member>()
                .HasKey(o => new { o.GroupId, o.UserId });

            //GroupInvitation - stworzenie klucza kompozytowego
            modelBuilder.Entity<GroupInvitation>()
                .HasKey(o => new { o.UserId, o.GroupId });
        }

        public DbSet<Group> Groups { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<KanbanTask> KanbanTasks { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<TaskComment> TaskComments { get; set; }
        public DbSet<GroupInvitation> Invitations { get; set; }
    }
}
