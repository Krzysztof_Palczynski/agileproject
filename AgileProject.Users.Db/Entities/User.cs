﻿using AgileProject.Users.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Users.Db.Entities
{
    public class User : IDbUser, IChangeUserDbInfo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordSalt { get; set; }


        //navigation properties
        public virtual ICollection<User> Colleagues { get; set; }

        ICollection<IDbUser> IDbUser.Colleagues => Colleagues as ICollection<IDbUser>;
    }
}
