﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AgileProject.Users.Db.Entities
{
    public class Colleague
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [ForeignKey(nameof(Colleague1))]
        public long Colleage1Id { get; set; }
        [ForeignKey(nameof(Colleague2))]
        public long Colleague2Id { get; set; }

        public bool AreColleagues(long user1Id, long user2Id)
        {
            return IsUserAColleague(user1Id) && IsUserAColleague(user2Id);
        }

        public bool IsUserAColleague(long userId)
        {
            return Colleage1Id == userId || Colleague2Id == userId;
        }

        //navigation properties
        public virtual User Colleague1 { get; set; }
        public virtual User Colleague2 { get; set; }
    }
}
