﻿using System;

namespace AgileProject.Groups.Services
{
    public class JwtTokenService : JwtService
    {
        protected const string _JwtSecret = "secretsecretsecretsecret";

        protected const string _JwtIssuer = "issuer";

        protected const string _JwtAudienceId = "audienceid";

        public JwtTokenService() : base(_JwtSecret, _JwtIssuer, _JwtAudienceId, TimeSpan.FromDays(1))
        {
        }
    }
}
