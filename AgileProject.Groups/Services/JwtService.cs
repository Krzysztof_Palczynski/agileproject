﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace AgileProject.Groups.Services
{
    public class JwtService
    {
        /// <summary>
        /// Line of text used for securing the message
        /// </summary>
        protected string JwtSecret { get; private set; }

        /// <summary>
        /// Line of text corresponding to "iss" parameter in JWTokens.
        /// It is a url to the issuer (without a method)
        /// </summary>
        protected string JwtIssuer { get; private set; }

        /// <summary>
        /// This is a sort of OAuth 2.0 clilent_id for JWT
        /// </summary>
        protected string JwtAudienceId { get; private set; }

        /// <summary>
        /// It determines after what period of time token should be disposed
        /// </summary>
        protected TimeSpan TokenLifespan { get; private set; }

        SymmetricSecurityKey signingKey;


        /// <param name="jwtSecret">Line of text used for securing the message</param>
        /// <param name="jwtIssuer">Line of text corresponding to "iss" parameter in JWTokens. It is a url to the issuer (without a method)</param>
        /// <param name="jwtAudienceId">This is a sort of OAuth 2.0 clilent_id for JWT</param>
        /// <param name="tokenLifespan">It determines after what period of time token should be disposed</param>
        public JwtService(string jwtSecret, string jwtIssuer, string jwtAudienceId, TimeSpan tokenLifespan)
        {
            JwtSecret = jwtSecret ?? throw new ArgumentNullException(nameof(jwtSecret));
            JwtIssuer = jwtIssuer ?? throw new ArgumentNullException(nameof(jwtIssuer));
            JwtAudienceId = jwtAudienceId ?? throw new ArgumentNullException(nameof(jwtAudienceId));
            TokenLifespan = tokenLifespan;

            signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtSecret));
        }

        protected virtual Claim[] _CreateClaims(long userId)
        {
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId.ToString(), ClaimValueTypes.Integer64),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64)
            };
            return claims;
        }

        /// <summary>
        /// Override this method to change paramters values
        /// </summary>
        /// <param name="signingKey"></param>
        /// <returns></returns>
        public virtual TokenValidationParameters CreateTokenValidationParameters()
        {
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = JwtIssuer,
                ValidateAudience = true,
                ValidAudience = JwtAudienceId,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true
            };
            return tokenValidationParameters;
        }

        public virtual string GetJWToken(long userId)
        {
            var now = DateTime.Now;

            var claims = _CreateClaims(userId);


            var tokenValidationParameters = CreateTokenValidationParameters();

            var jwt = new JwtSecurityToken(
                issuer: JwtIssuer,
                audience: JwtAudienceId,
                claims: claims,
                notBefore: now,
                expires: now.Add(TokenLifespan),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
    }
}
