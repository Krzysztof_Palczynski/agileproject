﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Logic.LocalStorage;
using AgileProject.Groups.Models.Classes;
using AgileProject.Groups.Models.Transport.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FilesController : ControllerBase
    {
        DbService db;
        FilesStore store;

        long userId => this.GetUserId();

        public FilesController(DbService db, FilesStore store)
        {
            this.db = db;
            this.store = store;
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<FileModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetGroupFiles(long groupId)
        {
            try
            {
                var result = db
                    .FilesList(userId, groupId)
                    .ConvertAll(x=>new FileModel(x));

                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(GroupPermissionDeniedException)
            {
                return Forbid();
            }
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(typeof(FileModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult PostFile([FromBody] PostFileModel file, [FromQuery] long groupId)
        {
            string serverFileName = "";
            try
            {
                serverFileName = store.CreateFile(file.GetBytes(), groupId, file.GetExtention());
                var result = db.FileCreateEntry(userId, groupId, file.FileName, serverFileName);
                var toreturn = new FileModel(result);
                return Ok(toreturn);
            }
            catch(GroupNotExistException)
            {
                store.DeleteFile(serverFileName, groupId);
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                store.DeleteFile(serverFileName, groupId);
                return Forbid();
            }
        }

        [HttpDelete]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult DeleteFile([FromQuery] long fileId)
        {
            try
            {
                var file = db.FileGet(userId, fileId);
                store.DeleteFile(file.NameOnServer, file.GroupId);
                db.FileDeleteEntry(userId, fileId);
                return Ok();
            }
            catch(FileNotExistException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(FileContentResult), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult DownloadFile([FromQuery] long fileId)
        {
            try
            {
                var file = db.FileGet(userId, fileId);
                var content = store.DownloadFile(file.NameOnServer, file.GroupId);
                return File(content, "application/octet-stream");
            }
            catch(FileNotExistException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }
    }
}