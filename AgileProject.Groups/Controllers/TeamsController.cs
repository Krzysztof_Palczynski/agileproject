﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Classes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        DbService Db;

        long userId
        {
            get => this.GetUserId();
        }

        public TeamsController(DbService db)
        {
            Db = db;
        }



        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<GroupModel>), 200)]
        public IActionResult GetUserGroups()
        {
            List<Group> groups = Db.GroupList(userId);
            var result = groups.ConvertAll(x => new GroupModel(x));
            return Ok(result);
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(typeof(long), 200)]
        public IActionResult CreateGroup([FromBody]GroupCreateModel model)
        {
            var result = Db.GroupCreate(userId, model);
            return Ok(result.Id);
        }

        [HttpPut]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult UpdateGroupInfo([FromBody]GroupCreateModel model, [FromQuery] long groupId)
        {
            try
            {
                var result = Db.GroupUdpateInfo(userId, groupId, model.Name, model.Description);
                return Ok();
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(GroupPermissionDeniedException)
            {
                return Forbid();
            }
        }

        [HttpDelete]
        [Route("[action]")]
        public IActionResult DeleteGroup([FromQuery]long groupId)
        {
            try
            {
                Db.GroupDelete(userId, groupId);
                return Ok();
            }
            catch (GroupNotExistException)
            {
                return NotFound();
            }
            catch (GroupPermissionDeniedException)
            {
                return Forbid();
            }
        }
    }
}