﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessagesController : ControllerBase
    {
        long userId => this.GetUserId();

        DbService db;

        public MessagesController(DbService db)
        {
            this.db = db;
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(typeof(Message), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult PostMessage([FromBody] MessageModel model, [FromQuery] long groupId)
        {
            try
            {
                var result = db.MessageCreate(userId, groupId, model.Text);
                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(MessagePermissionDeniedException)
            {
                return Forbid();
            }
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<Message>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetMessages([FromQuery] long groupId)
        {
            try
            {
                var result = db.MessagesList(userId, groupId);
                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(MessagePermissionDeniedException)
            {
                return Forbid();
            }
        }
    }
}