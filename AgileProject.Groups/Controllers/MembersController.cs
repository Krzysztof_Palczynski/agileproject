﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MembersController : ControllerBase
    {
        long userId => this.GetUserId();

        DbService db;

        public MembersController(DbService db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<long>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetMembers(long groupId)
        {
            try
            {
                var result = db.GroupMembersGet(userId, groupId)
                    .Select(x=>x.UserId)
                    .ToList();

                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }

        [HttpDelete]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult DeleteMember([FromQuery] long memberId, [FromQuery] long groupId)
        {
            try
            {
                db.GroupMemberDelete(userId, groupId, memberId);
                return Ok();
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(MemberNotExistException)
            {
                return NotFound();
            }
            catch(GroupPermissionDeniedException)
            {
                return Forbid();
            }
        }
    }
}