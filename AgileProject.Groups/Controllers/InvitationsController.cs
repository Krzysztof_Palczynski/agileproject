﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Classes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvitationsController : ControllerBase
    {
        DbService db;

        public long userId => this.GetUserId();

        public InvitationsController(DbService db)
        {
            this.db = db;
        }

        /// <summary>
        /// This method is for creator of group to see what invitations are pending
        /// </summary>
        [Route("[action]")]
        [HttpGet]
        [ProducesResponseType(typeof(List<InvitationModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetCreatedInvitations([FromQuery] long groupId)
        {
            try
            {
                var invitations = db.InvitationsGetToGroup(userId, groupId);
                var result = invitations.ConvertAll(x => new InvitationModel(x));
                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(GroupPermissionDeniedException)
            {
                return Forbid();
            }
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<InvitationModel>), 200)]
        public IActionResult GetUserInvitations()
        {
            var result = db.InvitationsGetUsers(userId);
            var toreturn = result.ConvertAll(x => new InvitationModel(x));
            return Ok(toreturn);
        }

        [HttpPut]
        [Route("[action]")]
        public IActionResult InviteUser(long invitedUserId, long groupId)
        {
            try
            {
                db.InvitationsCreate(userId, invitedUserId, groupId);
                return Ok();
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(InvitationCreatePermissionsException)
            {
                return Forbid();
            }
            catch(InvitationExistsException)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult DeleteInvitation(long invitedUserId, long groupId)
        {
            try
            {
                db.InvitationsDelete(userId, groupId, invitedUserId);
                return Ok();
            }
            catch(InvitationNotExistException)
            {
                return NotFound();
            }
            catch(InvitationDeleteDeniedException)
            {
                return Forbid();
            }
        }

        [HttpPut]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult AcceptInvitation([FromQuery] long groupId)
        {
            try
            {
                db.InvitationsAccept(userId, groupId);
                return Ok();
            }
            catch (InvitationNotExistException)
            {
                return NotFound();
            }
            catch (InvitationDeleteDeniedException)
            {
                return Forbid();
            }
        }
    }
}