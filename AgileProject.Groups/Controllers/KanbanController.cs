﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Classes;
using AgileProject.Groups.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class KanbanController : ControllerBase
    {
        long userId => this.GetUserId();

        DbService db;

        public KanbanController(DbService db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<KanbanTaskModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetTasks(long groupId)
        {
            try
            {
                var tasks = db.KanbanTasksList(userId, groupId)
                    .ConvertAll(x=> new KanbanTaskModel(x));

                return Ok(tasks);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(typeof(KanbanTaskModel), 200)]
        [ProducesResponseType(404)]
        public IActionResult CreateTask([FromBody]KanbanTaskCreateModel model)
        {
            try
            {
                var task = db.KanbanTaskCreate(userId, model.GroupId, model.PerformerId, model.Title, model.Description);
                var result = new KanbanTaskModel(task);
                return Ok(result);
            }
            catch(GroupNotExistException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }

        [HttpPut]
        [Route("[action]")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult ChangeState([FromQuery]long taskId, [FromQuery] TaskState state)
        {
            try
            {
                db.KanbanTaskChangeState(userId, taskId, state);
                return Ok();
            }
            catch(KanbanTaskNotExistsException)
            {
                return NotFound();
            }
            catch(UserNotRelatedToKanbanTaskException)
            {
                return Forbid();
            }
        }
    }
}