﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgileProject.Groups.Helpers;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Models.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AgileProject.Groups.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class KanbanTaskCommentsController : ControllerBase
    {
        long userId => this.GetUserId();

        DbService db;

        public KanbanTaskCommentsController(DbService db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("[action]")]
        [ProducesResponseType(typeof(List<TaskCommentModel>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult GetComments(long taskId)
        {
            try
            {
                var result = db.KanbanTaskCommentsList(userId, taskId)
                    .ConvertAll(x=>new TaskCommentModel(x));

                return Ok(result);
            }
            catch(KanbanTaskNotExistsException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }

        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType(typeof(TaskCommentModel), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(403)]
        public IActionResult CreateComment([FromQuery]KanbanTaskCommentCreateModel model)
        {
            try
            {
                var c = db.KanbanTaskCommentCreate(userId, model.TaskId, model.Text);
                var result = new TaskCommentModel(c);
                return Ok(result);
            }
            catch(KanbanTaskNotExistsException)
            {
                return NotFound();
            }
            catch(UserNotGroupMemberException)
            {
                return Forbid();
            }
        }
    }
}