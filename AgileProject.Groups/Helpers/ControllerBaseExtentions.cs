﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgileProject.Groups.Helpers
{
    public static class ControllerBaseExtentions
    {
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        public static long GetUserId(this ControllerBase controller)
        {
            var claims = controller?.User?.Claims;
            if (claims == null)
                throw new NullReferenceException("User's claims are null");
            
            var uid = claims.FirstOrDefault(x => x.Properties.Any(y => y.Value == "sub"))?.Value;
            if (string.IsNullOrEmpty(uid))
                throw new NullReferenceException("User does not have 'sub' property in token");

            if (long.TryParse(uid, out var id))
                return id;
            else
                throw new InvalidCastException($"Unable to parse id value: {uid} to long");
        }
    }
}
