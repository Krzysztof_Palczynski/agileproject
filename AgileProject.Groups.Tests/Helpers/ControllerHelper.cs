﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace AgileProject.Groups.Tests.Helpers
{
    public static class ControllerHelper
    {
        public static void AddUserToController(ControllerBase controller, long userId)
        {
            Claim claim = new Claim("string", userId.ToString());
            claim.Properties.Add("", "sub");

            controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext()
                {
                    User = new ClaimsPrincipal(new ClaimsIdentity(
                        new Claim[]
                        {
                            claim
                        }
                    ))
                }
            };
        }
    }
}
