﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface IChangeUserDbInfo
    {
        string Email { get; }
        string FirstName { get; }
        string LastName { get; }
    }
}
