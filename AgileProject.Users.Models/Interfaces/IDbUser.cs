﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface IDbUser
    {
        long Id { get; }
        string Email { get; }
        string FirstName { get; }
        string LastName { get; }
        string PasswordHash { get; }
        string PasswordSalt { get; }
        /// <summary>
        /// Users that are known to the user
        /// </summary>
        ICollection<IDbUser> Colleagues { get; }
    }

}
