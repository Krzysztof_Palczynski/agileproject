﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface ICreateDbUser : IUserLogin
    {
        string FirstName { get; }
        string LastName { get; }
    }
}
