﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface ICreateUser : ICreateDbUser
    {
        byte[] Thumbnail { get; }
    }
}
