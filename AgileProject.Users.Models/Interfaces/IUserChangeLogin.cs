﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface IUserChangeLogin : IUserLogin
    {
        string NewPassword { get; }
    }
}
