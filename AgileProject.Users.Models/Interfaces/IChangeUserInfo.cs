﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface IChangeUserInfo : IChangeUserDbInfo
    {
        byte[] Thumbnail { get; }
    }
}
