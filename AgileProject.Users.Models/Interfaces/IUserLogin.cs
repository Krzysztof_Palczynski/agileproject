﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Interfaces
{
    public interface IUserLogin
    {
        string Email { get; }
        string Password { get; }
    }
}
