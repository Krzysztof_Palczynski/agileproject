﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AgileProject.Users.Models.Classes.Transport
{
    public class FileTransport
    {
        public string Base64Content { get; set; }
        public string FileName { get; set; }

        public FileTransport()
        {

        }
        public FileTransport(byte[] array) : this()
        {
            SetContent(array);
        }

        public FileTransport(byte[] array, string fileName) : this(array)
        {
            FileName = fileName;
        }

        public void SetContent(byte[] array)
        {
            Base64Content = Convert.ToBase64String(array);
        }

        /// <exception cref="NullReferenceException"></exception>
        public byte[] GetByteArray()
        {
            if (Base64Content == null)
                throw new NullReferenceException($"{nameof(Base64Content)} is null");

            return Convert.FromBase64String(Base64Content);
        }
        public string GetExtention()
        {
            if(FileName == null)
                throw new NullReferenceException($"{nameof(FileName)} is null");

            return Path.GetExtension(FileName);
        }
    }
}
