﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Classes
{
    public class TokenResponse
    {
        public string JwtToken { get; set; }
    }
}
