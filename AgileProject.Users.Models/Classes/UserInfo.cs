﻿using AgileProject.Users.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileProject.Users.Models.Classes
{
    public class UserInfo : IChangeUserDbInfo
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<long> ColleaguesIds { get; set; }

        public UserInfo()
        {

        }
        public UserInfo(IDbUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user", "Parameter user is empty");

            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.ColleaguesIds = user.Colleagues?.Select(x => x.Id).ToList();
        }
    }
}
