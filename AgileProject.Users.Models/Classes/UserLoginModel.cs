﻿using AgileProject.Users.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Classes
{
    public class UserLoginModel : IUserLogin
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
