﻿using AgileProject.Users.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Users.Models.Classes
{
    public class ChangeUserInfoModel : IChangeUserDbInfo
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
