﻿using AgileProject.Users.Controllers;
using AgileProject.Users.Db;
using AgileProject.Users.Logic.Storage;
using AgileProject.Users.Models.Classes.Transport;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileProject.Tests.ControllerTests
{
    [TestClass]
    public class ImagesControllerTests
    {
        UsersContext context => TestResources.UsersContext;
        ImagesController con => TestResources.ImagesController;
        LocalStoreService store => TestResources.LocalStoreService;

        [TestCleanup]
        public void Clean()
        {
            TestResources.LocalStoreService.ClearImageStorage();
        }

        [TestMethod]
        public void SaveUserImage()
        {
            var c = gac();
            var file = TestFiles.Image1;

            c.PostUserImage(file.ToFileTransport());

            var user = context.Users.First();
            Assert.IsNotNull(store.GetUserImage(user.Id));
        }

        [TestMethod]
        public void GetUserImage()
        {
            var c = gac();
            var file = TestFiles.Image1;
            c.PostUserImage(file.ToFileTransport());

            var user = context.Users.First();
            var result = c.GetUserImage(user.Id) as OkObjectResult;

            var image = store.GetUserImage(user.Id);
            var received = (result.Value as FileTransport).GetByteArray();
            CollectionAssert.AreEqual(image, received);
        }

        [TestMethod]
        public void DeleteUserImage()
        {
            var c = gac();
            var file = TestFiles.Image1;
            c.PostUserImage(file.ToFileTransport());

            var result = c.DeleteUserImage();

            Assert.IsInstanceOfType(result, typeof(OkResult));
        }


        #region helpers
        /// <summary>
        /// gac - get authorized controller
        /// </summary>
        private ImagesController gac()
        {
            var c = con;
            TestResources.Db_Seed();
            var user = context.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        #endregion
    }
}
