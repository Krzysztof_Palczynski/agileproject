﻿using AgileProject.Groups.Controllers;
using AgileProject.Groups.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileProject.Tests.ControllerTests
{
    [TestClass]
    public class TeamsControllerTest
    {
        [TestCleanup]
        public void Clean()
        {
            TestResources.GroupsContext.Groups.RemoveRange(TestResources.GroupsContext.Groups);
        }

        [TestMethod]
        public void CreateGroup()
        {
            var c = gac();
            var model = new GroupCreateModel()
            {
                Name = "New group",
                Description = "Some description"
            };

            var result = c.CreateGroup(model);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            long groupId = (long)(result as OkObjectResult).Value;
            Assert.IsTrue(TestResources.GroupsContext.Groups.Any(x => x.Id == groupId));
        }

        [TestMethod]
        public void GetUserGroups()
        {
            CreateGroup();
            var c = gac();

            var groups = (c.GetUserGroups() as OkObjectResult).Value as List<GroupModel>;

            Assert.IsNotNull(groups);
            CollectionAssert.AllItemsAreNotNull(groups);
        }

        [TestMethod]
        public void UpdateInfo()
        {
            CreateGroup();
            var c = gac();
            var group = ((c.GetUserGroups() as OkObjectResult).Value as List<GroupModel>).First();
            var model = new GroupCreateModel()
            {
                Description = "editted descritpion",
                Name = "eddited name"
            };

            c.UpdateGroupInfo(model, group.Id);

            group = ((c.GetUserGroups() as OkObjectResult).Value as List<GroupModel>).First();
            Assert.AreEqual(group.Description, model.Description);
            Assert.AreEqual(group.Name, model.Name);
        }

        [TestMethod]
        public void Delete()
        {
            CreateGroup();
            var c = gac();
            var group = ((c.GetUserGroups() as OkObjectResult).Value as List<GroupModel>).First();

            c.DeleteGroup(group.Id);

            var groups = (c.GetUserGroups() as OkObjectResult).Value as List<GroupModel>;

            Assert.IsTrue(groups.Count == 0);
        }

        #region helpers
        /// <summary>
        /// gac - get authorized controller
        /// </summary>
        private TeamsController gac()
        {
            var c = new TeamsController(TestResources.GroupsDb);
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        #endregion
    }
}
