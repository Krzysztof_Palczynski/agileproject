﻿using AgileProject.Users.Controllers;
using AgileProject.Users.Db;
using AgileProject.Users.Models.Classes;
using AgileProject.Tests.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileProject.Tests.ControllerTests
{
    [TestClass]
    public class AuthorizationControllerTests
    {
        AuthorizationController con => TestResources.AuthorizationController;
        UsersContext context => TestResources.UsersContext;

        [TestCleanup]
        public void Clean()
        {
            TestResources.Db_Clear();
        }

        #region create-user
        [TestMethod]
        public void CreateUser()
        {
            var model = new CreateUserModel()
            {
                Email = "henryk@utp.edu.pl",
                FirstName = "Henryk",
                LastName = "Testowy",
                Password = "Password"
            };

            var result = con.CreateUser(model);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CreateUser_WrongEmail()
        {
            var model = new CreateUserModel()
            {
                Email = "henrykutp.edu.pl",
                FirstName = "Henryk",
                LastName = "Testowy",
                Password = "Password"
            };

            var result = con.CreateUser(model);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void CreateUser_WrongPassword()
        {
            var model = new CreateUserModel()
            {
                Email = "henry@kutp.edu.pl",
                FirstName = "Henryk",
                LastName = "Testowy",
                Password = "P   assword"
            };

            var result = con.CreateUser(model);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void CreateUser_ExistingEmail()
        {
            var model = new CreateUserModel()
            {
                Email = "henryk@utp.edu.pl",
                FirstName = "Henryk",
                LastName = "Testowy",
                Password = "Password"
            };

            con.CreateUser(model);
            var result = con.CreateUser(model);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }
        #endregion

        #region login
        [TestMethod]
        public void Login()
        {
            CreateUser();
            var model = new UserLoginModel()
            {
                Email = "henryk@utp.edu.pl",
                Password = "Password"
            };

            var r = con.Login(model);
            Assert.IsInstanceOfType(r, typeof(OkObjectResult));

            var result = (r as OkObjectResult).Value as TokenResponse;
            Assert.IsNotNull(result?.JwtToken);
        }

        [TestMethod]
        public void Login_WrongEmail()
        {
            var model = new UserLoginModel()
            {
                Email = "adgfsgdrsa",
                Password = "asdsfgnrer"
            };

            var r = con.Login(model);

            Assert.IsInstanceOfType(r, typeof(UnauthorizedResult));
        }
        #endregion
    }
}
