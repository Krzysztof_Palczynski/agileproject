﻿using AgileProject.Users.Controllers;
using AgileProject.Users.Db;
using AgileProject.Users.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AgileProject.Tests.ControllerTests
{
    [TestClass]
    public class ManagementControllerTest
    {
        UsersContext context => TestResources.UsersContext;

        ManagementController con => TestResources.ManagementController;

        [TestCleanup]
        public void Clean()
        {
            TestResources.Db_Clear();
        }

        #region delete-user
        [TestMethod]
        public void DeleteUser()
        {
            var c = gac();

            var result = c.DeleteUser();

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        [TestMethod]
        public void DeleteUser_userNotExists()
        {
            var c = gac();
            c.DeleteUser();

            var result = c.DeleteUser();

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
        #endregion

        #region get-user-info
        [TestMethod]
        public void GetUserInfo()
        {
            var c = gac();

            var result = c.GetUserInfo();

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsInstanceOfType((result as OkObjectResult).Value, typeof(UserInfo));
        }

        #endregion

        [TestMethod]
        public void ChangePassword()
        {
            var c = gac();
            var user = context.Users.First();
            var model = new UserChangeLoginModel() {
                Email = user.Email,
                Password = "password",
                NewPassword = "password1"
            };

            c.ChangePassword(model);

            var result = TestResources.AuthorizationController.Login(new UserLoginModel() {
                Email = user.Email,
                Password = "password1"
            }) as OkObjectResult;
            Assert.IsInstanceOfType(result.Value, typeof(TokenResponse));
        }

        [TestMethod]
        public void UpdateUserInfo()
        {
            var c = gac();
            var model = new ChangeUserInfoModel()
            {
                Email = "othermail@utp.edu.pl",
                FirstName = "Firstname",
                LastName = "Lastname"
            };

            c.UpdateUserInfo(model);

            var result = (c.GetUserInfo() as OkObjectResult).Value as UserInfo;

            Assert.AreEqual(model.Email, result.Email);
            Assert.AreEqual(model.FirstName, result.FirstName);
            Assert.AreEqual(model.LastName, result.LastName);
        }

        #region helpers
        /// <summary>
        /// gac - get authorized controller
        /// </summary>
        private ManagementController gac()
        {
            var c = con;
            TestResources.Db_Seed();
            var user = context.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }
        
        #endregion
    }
}
