﻿using AgileProject.Groups.Database.Context;
using AgileProject.Groups.Logic.LocalStorage;
using AgileProject.Users.Controllers;
using AgileProject.Users.Db;
using AgileProject.Users.Db.Entities;
using AgileProject.Users.Logic.Db;
using AgileProject.Users.Logic.Security;
using AgileProject.Users.Logic.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Tests.Resources
{
    public static class TestResources
    {
        public static string PathToLocalStore
        {
            get
            {
                return System.IO.Directory.GetCurrentDirectory();
            }
        }

        private static AuthorizationController authorizationController;
        public static AuthorizationController AuthorizationController
        {
            get
            {
                if (authorizationController == null)
                    authorizationController = new AuthorizationController(DbService);
                return authorizationController;
            }
        }

        private static UsersContext context;
        public static UsersContext UsersContext
        {
            get
            {
                if (context == null)
                {
                    var optBuildier = new DbContextOptionsBuilder<UsersContext>();
                    optBuildier.UseInMemoryDatabase(databaseName: "AgileProject.Users.Db");
                    context = new UsersContext(optBuildier.Options);
                }
                return context;
            }
        }

        private static GroupsContext groupsContext;
        public static GroupsContext GroupsContext
        {
            get
            {
                if(groupsContext == null)
                {
                    var optBuildier = new DbContextOptionsBuilder<GroupsContext>();
                    optBuildier.UseInMemoryDatabase(databaseName: "AgileProject.Groups.Db");
                    groupsContext = new GroupsContext(optBuildier.Options);
                }
                return groupsContext;
            }
        }

        public static DbService DbService { get => new DbService(UsersContext); }

        public static Groups.Logic.Database.DbService groupsDb;
        public static Groups.Logic.Database.DbService GroupsDb
        {
            get
            {
                if(groupsDb == null)
                {
                    groupsDb = new Groups.Logic.Database.DbService(GroupsContext);
                }
                return groupsDb;
            }
        }

        private static LocalStoreService localStoreService;
        public static LocalStoreService LocalStoreService
        {
            get
            {
                if(localStoreService == null)
                {
                    localStoreService = new LocalStoreService(PathToLocalStore);
                }
                return localStoreService;
            }
        }

        private static FilesStore groupFilesStore;
        public static FilesStore GroupsFileStore
        {
            get
            {
                if (groupFilesStore == null)
                {
                    groupFilesStore = new FilesStore(PathToLocalStore);
                }
                return groupFilesStore;
            }
        }

        public static ManagementController managementController;
        public static ManagementController ManagementController
        {
            get
            {
                if (managementController == null)
                {
                    managementController = new ManagementController(DbService);
                }
                return managementController;
            }
        }

        private static ImagesController imagesController;
        public static ImagesController ImagesController
        {
            get
            {
                if (imagesController == null)
                    imagesController = new ImagesController(LocalStoreService);
                return imagesController;
            }
        }



        public static void Db_Seed()
        {
            string random () { return Guid.NewGuid().ToString(); }

            //Seeding users
            List<User> users = new List<User>();
            var r = new Random();
            var salt = Hashing.GenerateSalt();
            var hash = Hashing.HashPassword("password", salt);

            for (int i = 0; i < 3; i++)
            {
                users.Add(new User() {
                    Id = r.Next(),
                    Email = $"{random()}@utp.edu.pl",
                    FirstName = random(),
                    LastName = random(),
                    PasswordHash = hash,
                    PasswordSalt = Convert.ToBase64String(salt)
                });
            }
            users.ForEach(x => UsersContext.Entry(x).State = EntityState.Added);

            //Seeding colleagues
            UsersContext.Entry(new Colleague() {
                Colleage1Id = users[0].Id,
                Colleague2Id = users[1].Id
            }).State = EntityState.Added;

            UsersContext.SaveChanges();
        }
        public static void Db_Clear()
        {
            UsersContext.Colleagues.RemoveRange(UsersContext.Colleagues);
            UsersContext.Users.RemoveRange(UsersContext.Users);

            UsersContext.SaveChanges();
        }

    }
}
