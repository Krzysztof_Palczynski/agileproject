﻿using AgileProject.Groups.Models.Transport.Classes;
using AgileProject.Users.Models.Classes.Transport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AgileProject.Tests.Resources
{
    public static class TestFiles
    {
        private static TestFile image1;
        public static TestFile Image1
        {
            get
            {
                if(image1 == null)
                {
                    image1 = createTestFile("1.jpg");
                }
                return image1;
            }
        }



        private static string pathToFile(string filename)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "Resources", "Files", filename);
        }
        private static TestFile createTestFile(string filename)
        {
            var bytes = File.ReadAllBytes(pathToFile(filename));
            return new TestFile(bytes, filename);
        }
    }

    public class TestFile
    {
        public byte[] Content { get; }
        public string FileName { get; }
        public string Extention => Path.GetExtension(FileName);

        public TestFile(byte[] content, string filename)
        {
            Content = content;
            FileName = filename;
        }

        public string GetBase64Encoded()
        {
            return Convert.ToBase64String(Content);
        }

        public FileTransport ToFileTransport()
        {
            return new FileTransport(Content, FileName);
        }

        public PostFileModel ToPostFileModel()
        {
            return new PostFileModel()
            {
                Base64Content = GetBase64Encoded(),
                FileName = FileName
            };
        }
    }
}
