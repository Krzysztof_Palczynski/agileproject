﻿using AgileProject.Users.Db;
using AgileProject.Users.Logic.Db;
using AgileProject.Users.Logic.Exceptions;
using AgileProject.Users.Logic.Security;
using AgileProject.Users.Models.Classes;
using AgileProject.Tests.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileProject.Tests.WhiteTests
{
    [TestClass]
    public class DbServiceTests
    {
        public static UsersContext Context => TestResources.UsersContext;
        public static DbService DbService => TestResources.DbService;

        [TestCleanup]
        public void Clean()
        {
            TestResources.Db_Clear();
        }

        #region create-user-tests
        [TestMethod]
        public void CreateUser()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            Assert.IsTrue(Context.Users.Any(x => x.Id == result.Id));
        }

        [TestMethod]
        public void CreateUser_WrongEmail()
        {
            var user = new CreateUserModel()
            {
                Email = "userutp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            Assert.ThrowsException<InvalidEmailException>(() => DbService.CreateUser(user));
        }

        [TestMethod]
        public void CreateUser_WrongPassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "pass word",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            Assert.ThrowsException<InvalidPasswordException>(() => DbService.CreateUser(user));
        }

        [TestMethod]
        public void CreateUser_SameEmail_ShouldThrowException()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            DbService.CreateUser(user);

            Assert.ThrowsException<UserExistsException>(() => DbService.CreateUser(user));
        }

        #endregion

        #region challenge-user-tests
        [TestMethod]
        public void ChallengeUser()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            var model = new UserLoginModel()
            {
                Email = user.Email,
                Password = user.Password
            };

            var c = DbService.ChallengeUser(model);

            Assert.IsTrue(c.Id == result.Id);
        }

        [TestMethod]
        public void ChallengeUser_WrongPassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            var model = new UserLoginModel()
            {
                Email = user.Email,
                Password = user.Password + "1"
            };

            var c = DbService.ChallengeUser(model);

            Assert.IsTrue(c == null);
        }

        [TestMethod]
        public void ChallengeUser_NumbersInPassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "0p1a2s3s4w5o6r7d8",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            var model = new UserLoginModel()
            {
                Email = user.Email,
                Password = user.Password
            };

            var c = DbService.ChallengeUser(model);

            Assert.IsTrue(c.Id == result.Id);
        }

        [TestMethod]
        public void ChallengeUser_CapitalLettersInPassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "PaSsWoRd",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            var model = new UserLoginModel()
            {
                Email = user.Email,
                Password = user.Password
            };

            var c = DbService.ChallengeUser(model);

            Assert.IsTrue(c.Id == result.Id);
        }
    

        #endregion

        #region change-password-test
        [TestMethod]
        public void ChangePassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            string newPassword = "password1";
            var result2 = DbService.ChangePassword(user, newPassword);

            var model = new UserLoginModel()
            {
                Email = user.Email,
                Password = newPassword
            };
            Assert.IsNotNull(DbService.ChallengeUser(model));
        }

        [TestMethod]
        public void ChangePassword_WrongCredentialsDuringChanging()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            string newPassword = "password1";
            user.Password = "wrongPassword";
            Assert.ThrowsException<UserLoginFailedException>(() => DbService.ChangePassword(user, newPassword));
        }

        public void ChangePassword_WrongPassword()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            string newPassword = "a";
            Assert.ThrowsException<InvalidPasswordException>(() => DbService.ChangePassword(user, newPassword));
        }
        #endregion

        #region change-user-info-tests
        [TestMethod]
        public void ChangeUserInfo()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);

            var model = new ChangeUserInfoModel()
            {
                Email = "a" + user.Email,
                FirstName = "A" + user.FirstName,
                LastName = "B" + user.LastName
            };

            DbService.ChangeUserInfo(result.Id, model);

            var dbUser = DbService.GetUserInfo(result.Id);

            Assert.AreSame(dbUser.Email, model.Email, "Email is not the same");
            Assert.AreSame(dbUser.FirstName, model.FirstName, "FirstName is not the same");
            Assert.AreSame(dbUser.LastName, model.LastName, "LastName is not the same");

        }

        [TestMethod]
        public void ChangeUserInfo_WrongEmail()
        {
            TestResources.Db_Clear();
            TestResources.Db_Seed();
            var user = Context.Users.First();

            user.Email = "wrong email";

            Assert.ThrowsException<InvalidEmailException>(() => DbService.ChangeUserInfo(user.Id, user));
        }

        [TestMethod]
        public void ChangeUserInfo_UserNotExists()
        {
            var model = new ChangeUserInfoModel()
            {
                Email = "user@utp.edu.pl",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            Assert.ThrowsException<UserNotExistsException>(() => DbService.ChangeUserInfo(1234567890, model));
        }
        #endregion

        #region delete-user
        [TestMethod]
        public void DeleteUser()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var result = DbService.CreateUser(user);
            var id = result.Id;

            DbService.DeleteUser(id);

            Assert.IsFalse(Context.Users.Any(x => x.Id == id));
        }

        [TestMethod]
        public void DeleteUser_UserNotExist()
        {
            Assert.ThrowsException<UserNotExistsException>(() => DbService.DeleteUser(1234567890));
        }
        #endregion

        #region get-user-info-test
        [TestMethod]
        public void GetUserInfo()
        {
            var user = new CreateUserModel()
            {
                Email = "user@utp.edu.pl",
                Password = "password",
                FirstName = "Henryk",
                LastName = "Testowy"
            };

            var r = DbService.CreateUser(user);

            var result = DbService.GetUserInfo(r.Id);

            Assert.AreSame(user.Email, result.Email, "Email is not the same");
            Assert.AreSame(user.FirstName, result.FirstName, "FirstName is not the same");
            Assert.AreSame(user.LastName, result.LastName, "LastName is not the same");

        }

        [TestMethod]
        public void GetUserInfo_UserNotExists()
        {
            Assert.ThrowsException<UserNotExistsException>(() => DbService.GetUserInfo(1234567890));
        }

        #endregion

        #region get-users-tests
        [TestMethod]
        public void GetUsers()
        {
            var users = DbService.GetUsers();

            Assert.AreEqual(users.Count, Context.Users.Count());
            Assert.IsFalse(users.Any(x => String.IsNullOrEmpty(x.Email)));
            Assert.IsFalse(users.Any(x => String.IsNullOrEmpty(x.FirstName)));
            Assert.IsFalse(users.Any(x => String.IsNullOrEmpty(x.LastName)));

        }
        #endregion
    }
}
