﻿using AgileProject.Users.Logic.Exceptions;
using AgileProject.Users.Logic.Storage;
using AgileProject.Tests.Resources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Tests.WhiteTests
{
    [TestClass]
    public class LocalStoreServiceTests
    {
        LocalStoreService store => TestResources.LocalStoreService;
        TestFile f => TestFiles.Image1;

        [TestCleanup]
        public void Clean()
        {
            store.ClearImageStorage();
        }

        #region save-image-tests
        [TestMethod]
        public void SaveImage()
        {
            var file = f;

            store.SaveUserImage(1, f.Content, f.Extention);

            var image = store.GetUserImage(1);
            CollectionAssert.AreEqual(f.Content, image);
        }

        [TestMethod]
        public void DeleteImage()
        {
            var file = f;
            store.SaveUserImage(1, f.Content, f.Extention);

            store.DeleteUserImage(1);

            Assert.ThrowsException<ImageNotExistsException>(() => store.GetUserImage(1));
        }

        [TestMethod]
        public void GetImage_StoreNotExists()
        {
            Assert.ThrowsException<ImageStoreNotExists>(() => store.GetUserImage(1234567890));
        }
        #endregion
    }
}
