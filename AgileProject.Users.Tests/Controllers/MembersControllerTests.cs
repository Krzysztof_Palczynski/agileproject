using AgileProject.Groups.Controllers;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using AgileProject.Users.Db.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class MembersControllerTests
    {
        public long UserId { get; private set; }
        public long PerformerId { get; private set; }
        public long GroupId { get; private set; }

        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            PerformerId = TestResources.UsersContext.Users.Last().Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
            Member m = new Member()
            {
                GroupId = GroupId,
                UserId = UserId
            };
            Member mm = new Member()
            {
                GroupId = GroupId,
                UserId = PerformerId
            };
            TestResources.GroupsContext.Entry(m).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.Entry(mm).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.SaveChanges();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsContext.Groups.RemoveRange(TestResources.GroupsContext.Groups);
        }

        private MembersController CreateMembersController()
        {
            var c = new MembersController(
                TestResources.GroupsDb);

            User user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void GetMembers_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateMembersController();
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.GetMembers(
                groupId) as OkObjectResult).Value as List<long>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void DeleteMember_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateMembersController();
            long memberId = PerformerId;
            long groupId = GroupId;

            // Act
            var result = unitUnderTest.DeleteMember(
                memberId,
                groupId);

            // Assert
            Assert.IsFalse(TestResources.GroupsContext.Members.Any(x => x.UserId == memberId));
        }
    }
}
