using AgileProject.Groups.Controllers;
using AgileProject.Groups.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using AgileProject.Users.Db.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class InvitationsControllerTests
    {
        public long UserId { get; private set; }
        public long InvitedId { get; set; }
        public long GroupId { get; private set; }

        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            InvitedId = TestResources.UsersContext.Users.Last().Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsContext.Invitations.RemoveRange(TestResources.GroupsContext.Invitations);
        }

        private InvitationsController CreateInvitationsController(bool sender)
        {
            var c = new InvitationsController(
                TestResources.GroupsDb);


            User user = sender ? TestResources.UsersContext.Users.First() : TestResources.UsersContext.Users.Last();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void GetCreatedInvitations_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            InviteUser_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateInvitationsController(true);
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.GetCreatedInvitations(
                groupId) as OkObjectResult).Value as List<InvitationModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void GetUserInvitations_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            InviteUser_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateInvitationsController(false);

            // Act
            var result = (unitUnderTest.GetUserInvitations() as OkObjectResult).Value as List<InvitationModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void InviteUser_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateInvitationsController(true);
            long invitedUserId = InvitedId;
            long groupId = GroupId;

            // Act
            var result = unitUnderTest.InviteUser(
                invitedUserId,
                groupId);

            // Assert
            var inv = TestResources.GroupsContext.Invitations;
            Assert.IsTrue(inv.Any(x => x.UserId == invitedUserId));
        }

        [TestMethod]
        public void DeleteInvitation_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            InviteUser_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateInvitationsController(true);
            long invitedUserId = InvitedId;
            long groupId = GroupId;

            // Act
            var result = unitUnderTest.DeleteInvitation(
                invitedUserId,
                groupId);

            // Assert
            Assert.IsTrue(TestResources.GroupsDb.InvitationsGetToGroup(UserId, GroupId).Count == 0);
        }

        [TestMethod]
        public void AcceptInvitation()
        {
            InviteUser_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateInvitationsController(false);

            unitUnderTest.AcceptInvitation(GroupId);

            Assert.IsTrue(TestResources.GroupsContext.Members.Any(x => x.GroupId == GroupId && x.UserId == InvitedId));
            Assert.IsFalse(TestResources.GroupsContext.Invitations.Any(x => x.GroupId == GroupId && x.UserId == InvitedId));
        }

    }
}
