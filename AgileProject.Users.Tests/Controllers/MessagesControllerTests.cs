using AgileProject.Groups.Controllers;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using AgileProject.Users.Db.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class MessagesControllerTests
    {
        public long UserId { get; private set; }
        public long GroupId { get; private set; }

        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
            Member m = new Member()
            {
                GroupId = GroupId,
                UserId = UserId
            };
            TestResources.GroupsContext.Entry(m).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.SaveChanges();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsContext.Groups.RemoveRange(TestResources.GroupsContext.Groups);
        }

        private MessagesController CreateMessagesController()
        {
            var c = new MessagesController(
                TestResources.GroupsDb);

            User user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void PostMessage_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateMessagesController();
            MessageModel model = new MessageModel() {
                Text = "Some text"
            };
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.PostMessage(
                model,
                groupId) as OkObjectResult).Value as Message;

            // Assert
            Assert.IsTrue(TestResources.GroupsContext.Messages.Any(x => x.Id == result.Id));
        }

        [TestMethod]
        public void GetMessages_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            PostMessage_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateMessagesController();
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.GetMessages(
                groupId) as OkObjectResult).Value as List<Message>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }
    }
}
