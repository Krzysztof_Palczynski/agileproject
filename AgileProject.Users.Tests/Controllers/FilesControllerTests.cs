using AgileProject.Groups.Controllers;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Logic.Exceptions;
using AgileProject.Groups.Logic.LocalStorage;
using AgileProject.Groups.Models.Classes;
using AgileProject.Groups.Models.Transport.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class FilesControllerTests
    {
        public long GroupId = 0;
        public long UserId = 0;

        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsFileStore.DeleteStore();
        }

        private FilesController CreateFilesController()
        {
            var c = new FilesController(
                TestResources.GroupsDb,
                TestResources.GroupsFileStore);
            var user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void GetGroupFiles_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            PostFile_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateFilesController();
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.GetGroupFiles(
                groupId) as OkObjectResult).Value as List<FileModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void PostFile_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateFilesController();
            PostFileModel file = TestFiles.Image1.ToPostFileModel();
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.PostFile(
                file,
                groupId) as OkObjectResult).Value as FileModel;

            // Assert
            var dbfile = TestResources.GroupsDb.FileGet(UserId, result.Id);
            Assert.IsNotNull(dbfile);
            var bytes = TestResources.GroupsFileStore.DownloadFile(dbfile.NameOnServer, GroupId);
            Assert.IsNotNull(bytes);
        }

        [TestMethod]
        public void DeleteFile_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            PostFile_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateFilesController();
            long fileId = TestResources.GroupsDb.FilesList(UserId, GroupId).First().Id;

            // Act
            var result = unitUnderTest.DeleteFile(
                fileId);

            // Assert
            Assert.ThrowsException<FileNotExistException>(() => TestResources.GroupsDb.FileGet(UserId, fileId));
        }

        [TestMethod]
        public void DownloadFile_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            PostFile_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateFilesController();
            long fileId = TestResources.GroupsDb.FilesList(UserId, GroupId).First().Id;


            // Act
            var result = unitUnderTest.DownloadFile(
                fileId) as FileContentResult;

            // Assert
            Assert.IsNotNull(result.FileContents);
        }
    }
}
