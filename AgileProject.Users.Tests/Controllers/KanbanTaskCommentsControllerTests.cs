using AgileProject.Groups.Controllers;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Models.Classes;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using AgileProject.Users.Db.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class KanbanTaskCommentsControllerTests
    {
        public long UserId { get; private set; }
        public long PerformerId { get; private set; }
        public long GroupId { get; private set; }
        public long TaskId { get; private set; }

        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            PerformerId = TestResources.UsersContext.Users.Last().Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
            Member m = new Member()
            {
                GroupId = GroupId,
                UserId = UserId
            };
            Member mm = new Member()
            {
                GroupId = GroupId,
                UserId = PerformerId
            };
            TestResources.GroupsContext.Entry(m).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.Entry(mm).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.SaveChanges();
            TaskId = TestResources.GroupsDb.KanbanTaskCreate(UserId, GroupId, PerformerId, "T", "D").Id;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsContext.Groups.RemoveRange(TestResources.GroupsContext.Groups);
        }

        private KanbanTaskCommentsController CreateKanbanTaskCommentsController()
        {
            var c = new KanbanTaskCommentsController(
                TestResources.GroupsDb);

            User user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void GetComments_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            CreateComment_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateKanbanTaskCommentsController();
            long taskId = TaskId;

            // Act
            var result = (unitUnderTest.GetComments(
                taskId) as OkObjectResult).Value as List<TaskCommentModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void CreateComment_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateKanbanTaskCommentsController();
            KanbanTaskCommentCreateModel model = new KanbanTaskCommentCreateModel()
            {
                TaskId = TaskId,
                Text = "Comment"
            };

            // Act
            var result = (unitUnderTest.CreateComment(
                model) as OkObjectResult).Value as TaskCommentModel;

            // Assert
            Assert.IsTrue(TestResources.GroupsContext.TaskComments.Any(x => x.Id == result.Id));
        }
    }
}
