using AgileProject.Groups.Controllers;
using AgileProject.Groups.Database.Entities;
using AgileProject.Groups.Logic.Database;
using AgileProject.Groups.Models.Classes;
using AgileProject.Groups.Models.Database;
using AgileProject.Tests.Helpers;
using AgileProject.Tests.Resources;
using AgileProject.Users.Db.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AgileProject.Tests.Controllers
{
    [TestClass]
    public class KanbanControllerTests
    {
        public long UserId { get; private set; }
        public long GroupId { get; private set; }
        public long PerformerId { get; set; }


        [TestInitialize]
        public void TestInitialize()
        {
            var g = new GroupCreateModel()
            {
                Description = "d",
                Name = "n"
            };
            TestResources.Db_Seed();
            var user = TestResources.UsersContext.Users.First();
            UserId = user.Id;
            PerformerId = TestResources.UsersContext.Users.Last().Id;
            GroupId = TestResources.GroupsDb.GroupCreate(user.Id, g).Id;
            Member m = new Member()
            {
                GroupId = GroupId,
                UserId = UserId
            };
            Member mm = new Member()
            {
                GroupId = GroupId,
                UserId = PerformerId
            };
            TestResources.GroupsContext.Entry(m).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.Entry(mm).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            TestResources.GroupsContext.SaveChanges();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestResources.GroupsContext.Groups.RemoveRange(TestResources.GroupsContext.Groups);
        }

        private KanbanController CreateKanbanController()
        {
            var c = new KanbanController(
                TestResources.GroupsDb);

            User user = TestResources.UsersContext.Users.First();
            ControllerHelper.AddUserToController(c, user.Id);
            return c;
        }

        [TestMethod]
        public void GetTasks_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            CreateTask_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateKanbanController();
            long groupId = GroupId;

            // Act
            var result = (unitUnderTest.GetTasks(
                groupId) as OkObjectResult).Value as List<KanbanTaskModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void CreateTask_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var unitUnderTest = this.CreateKanbanController();
            KanbanTaskCreateModel model = new KanbanTaskCreateModel()
            {
                Description = "d",
                GroupId = GroupId,
                PerformerId = PerformerId,
                State = Groups.Models.Database.TaskState.InProgress,
                Title = "T"
            };

            // Act
            var result = (unitUnderTest.CreateTask(
                model) as OkObjectResult).Value as KanbanTaskModel;

            // Assert
            Assert.IsTrue(TestResources.GroupsContext.KanbanTasks.Any(x => x.Id == result.Id));
        }

        [TestMethod]
        public void ChangeState_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            CreateTask_StateUnderTest_ExpectedBehavior();
            var unitUnderTest = this.CreateKanbanController();
            long taskId = TestResources.GroupsContext.KanbanTasks.First().Id;
            TaskState state = TaskState.Finished;

            // Act
            var result = unitUnderTest.ChangeState(
                taskId,
                state);

            // Assert
            Assert.AreEqual(TestResources.GroupsContext.KanbanTasks.First().State, state);
        }
    }
}
