﻿using AgileProject.Users.Client;
using AgileProject.Users.Models.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AgileProject.Tests.IntegrationTests
{
    //[TestClass]
    public class UsersClientTests
    {
        UsersClient client
        {
            get { return new UsersClient(); }
        }

        //[TestMethod]
        public void CreateUser_LogIn_Delete()
        {
            string email = $"{Guid.NewGuid().ToString()}@utp.edu.pl";
            string password = "password";
            var client = this.client;
            var user = new CreateUserModel()
            {
                Email = email,
                FirstName = "Kazimierz",
                LastName = "Z UTP",
                Password = password
            };

            var result = client.CreateUser(user);
            Assert.IsTrue(result);

            var login = new UserLoginModel() { Email = email, Password = password };
            var token = client.Login(login);
            Assert.IsTrue(token?.JwtToken != null);

            var deleteResult = client.DeleteUser();
            Assert.IsTrue(result);

            Assert.ThrowsException<UnauthorizedAccessException>(() => client.Login(login));
        }
    }
}
